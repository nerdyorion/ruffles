<?php defined('BASEPATH') OR exit('No direct script access allowed');
class University_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $title = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(universities.id) AS count FROM universities LEFT JOIN countries ON universities.country_id = countries.id WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (universities.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(universities.name LIKE '". $title_full . "') OR (countries.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (universities.name LIKE '". $title_word . "') OR (countries.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $title = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT universities.id, universities.country_id, universities.name, countries.name AS country_name FROM universities LEFT JOIN countries ON universities.country_id = countries.id WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (universities.id = '". $id . "')";
            }
        }

        if($title !== FALSE)
        {
            $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            $where .= "(universities.name LIKE '". $title_full . "') OR (countries.name LIKE '". $title_full . "')";

            $title_array = explode(' ', $title);
            if(count($title_array) > 1)
            {
                foreach ($title_array as $title_word) {
                    if(!empty($title_word))
                    {
                        $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        $where .= " OR (universities.name LIKE '". $title_word . "') OR (countries.name LIKE '". $title_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY universities.name ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count() {
        return $this->db->count_all("universities");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('universities.id, universities.country_id, universities.name, countries.name AS country_name');
            $this->db->from('universities'); 
            $this->db->join('countries', 'universities.country_id = countries.id', 'left');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();  // echo $this->db->last_query(); die;
            return $query->result_array();
        }

        $this->db->select('universities.id, universities.country_id, universities.name, countries.name AS country_name');
        $this->db->from('universities'); 
        $this->db->join('countries', 'universities.country_id = countries.id', 'left');
        $this->db->where('universities.id', (int) $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name');
            $this->db->from('universities'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name');
        $this->db->from('universities');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsByCountryIDDropDown($country_id)
    {
        $this->db->order_by('name', 'ASC');
        $this->db->select('id, name');
        $this->db->from('universities');
        $this->db->where('country_id', $country_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRandom($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('id', 'RANDOM');
        $this->db->select('id, name');
        $this->db->from('universities');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }


    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'country_id' => (int) $this->input->post('country_id'),
            'name' => trim($this->input->post('name')),
            'created_by' => $created_by
        );

        $this->db->insert('universities', $data);
    }

    public function delete($id)
    {
        $this->db->delete('universities', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'country_id' => (int) $this->input->post('country_id'),
            'name' => trim($this->input->post('name'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('universities', $data);
    }
}