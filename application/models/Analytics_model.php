<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Analytics_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function getRows($id = FALSE)
    {
        if ($id === FALSE)
        {
            //$query = $this->db->get('dnd_users');
            //return $query->result_array();
            $query = $this->db->order_by('users.full_name', 'ASC');
            $this->db->select('rbt_analytics.id, users.full_name, users.username, users.stage_name, rbt_analytics.user_id, rbt_analytics.count_lagos, rbt_analytics.count_north1, rbt_analytics.count_north2, rbt_analytics.count_south_east, rbt_analytics.count_south_south, rbt_analytics.count_south_west, rbt_analytics.count_total');
            $this->db->from('rbt_analytics'); 
            $this->db->join('users', 'rbt_analytics.user_id = users.id', 'left');
            $query = $this->db->get(); //echo $this->db->last_query(); die;

            return $query->result_array();
        }
        $this->db->select('rbt_analytics.id, users.full_name, users.username, users.stage_name, rbt_analytics.user_id, rbt_analytics.count_lagos, rbt_analytics.count_north1, rbt_analytics.count_north2, rbt_analytics.count_south_east, rbt_analytics.count_south_south, rbt_analytics.count_south_west, rbt_analytics.count_total');
        $this->db->from('rbt_analytics'); 
        $this->db->join('users', 'rbt_analytics.user_id = users.id', 'left');
        $this->db->where('rbt_analytics.id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowByTalentID($id)
    {
        $this->db->select('rbt_analytics.id, users.full_name, users.username, users.stage_name, rbt_analytics.user_id, rbt_analytics.count_lagos, rbt_analytics.count_north1, rbt_analytics.count_north2, rbt_analytics.count_south_east, rbt_analytics.count_south_south, rbt_analytics.count_south_west, rbt_analytics.count_total');
        $this->db->from('rbt_analytics'); 
        $this->db->join('users', 'rbt_analytics.user_id = users.id', 'left');
        $this->db->where('rbt_analytics.user_id', (int) $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();    //echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function add()
    {
        $count_lagos = (int) $this->input->post('count_lagos');
        $count_north1 = (int) $this->input->post('count_north1');
        $count_north2 = (int) $this->input->post('count_north2');
        $count_south_east = (int) $this->input->post('count_south_east');
        $count_south_south = (int) $this->input->post('count_south_south');
        $count_south_west = (int) $this->input->post('count_south_west');
        $count_total = $count_lagos + $count_north1 + $count_north2 + $count_south_east + $count_south_south + $count_south_west;

        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'user_id' => (int) $this->input->post('user_id'),
            'count_lagos' => $count_lagos,
            'count_north1' => $count_north1,
            'count_north2' => $count_north2,
            'count_south_east' => $count_south_east,
            'count_south_south' => $count_south_south,
            'count_south_west' => $count_south_west,
            'count_total' => $count_total,
            'created_by' => $created_by
        );

        $sql = 'INSERT INTO rbt_analytics (user_id, count_lagos, count_north1, count_north2, count_south_east, count_south_south, count_south_west, count_total, created_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
        ON DUPLICATE KEY UPDATE 
        count_lagos=VALUES(count_lagos) + count_lagos, 
        count_north1=VALUES(count_north1) + count_north1, 
        count_north2=VALUES(count_north2) + count_north2,  
        count_south_east=VALUES(count_south_east) + count_south_east,
        count_south_south=VALUES(count_south_south) + count_south_south,
        count_south_west=VALUES(count_south_west) + count_south_west,
        count_total=VALUES(count_total) + count_total,
        last_updated=NOW()';

        $query = $this->db->query($sql, $data);
    }

    public function update($id)
    {
        $count_lagos = (int) $this->input->post('count_lagos');
        $count_north1 = (int) $this->input->post('count_north1');
        $count_north2 = (int) $this->input->post('count_north2');
        $count_south_east = (int) $this->input->post('count_south_east');
        $count_south_south = (int) $this->input->post('count_south_south');
        $count_south_west = (int) $this->input->post('count_south_west');
        $count_total = $count_lagos + $count_north1 + $count_north2 + $count_south_east + $count_south_south + $count_south_west;

        $data = array(
            'count_lagos' => $count_lagos,
            'count_north1' => $count_north1,
            'count_north2' => $count_north2,
            'count_south_east' => $count_south_east,
            'count_south_south' => $count_south_south,
            'count_south_west' => $count_south_west,
            'count_total' => $count_total,
        );
        $this->db->where('id', (int) $id);
        $this->db->update('rbt_analytics', $data);
    }

    public function delete($id)
    {
        $this->db->delete('rbt_analytics', array('id' => (int) $id));
    }
}