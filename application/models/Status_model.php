<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Status_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function record_count() {
        return $this->db->count_all("status");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, description');
            $this->db->from('status'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('id, name, description');
        $this->db->from('status');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name, description');
            $this->db->from('status'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name');
        $this->db->from('status');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
}