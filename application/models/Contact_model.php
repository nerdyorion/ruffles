<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// ini_set('max_execution_time', 3600); //3600 seconds = 60 minutes
 set_time_limit(0);

class Contact_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function getRows($id = FALSE)
    {
        if ($id === FALSE)
        {
            //$query = $this->db->get('SMSC_dnd');
            //return $query->result_array();
            $query = $this->db->order_by('date_created', 'DESC');
            $this->db->from('SMSC_dnd'); 
            $query = $this->db->limit(20);
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $query = $this->db->get_where('SMSC_dnd', array('id' => $id));
        return $query->row_array();
    }

    public function getCount($type)
    {
        if ($type == "ALL")
        {
            //return $this->db->count_all('SMSC_dnd');
            $this->db->select('total'); 
            $this->db->from('summary');   
            $this->db->where('category', 'dnd');
            return $this->db->get()->row()->total;
            /*
            $this->db->select('total'); 
    	    $this->db->from('summary');   
    	    $this->db->where('category', 'dnd');
            return $this->db->get()->row()->total;
            */
        }
        elseif ($type == "TODAY")
        {
            /*
            $sql = "SELECT COUNT(id) AS count FROM `SMSC_dnd` WHERE DATE(`date_created`) = CURDATE()";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];

            $this->db->select('today'); 
            $this->db->from('summary');   
            $this->db->where('category', 'dnd');
            return $this->db->get()->row()->today;
            */
            $sql = "SELECT `today` AS today FROM `summary` WHERE `category` = 'dnd' AND DATE(`date_updated`) = CURDATE()";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['today'];
        }
        else
        {
            return 0;
        }
    }

    public function getCountOld($type)
    {
        if ($type == "ALL")
        {
            return $this->db->count_all('SMSC_dnd');
        }
        elseif ($type == "TODAY")
        {
            $sql = "SELECT COUNT(id) AS count FROM `SMSC_dnd` WHERE DATE(`date_created`) = CURDATE()";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }
        else
        {
            return 0;
        }
    }

    public function getRowByMSISDN()
    {
    	$msisdn = $this->input->post('msisdn');
        $this->db->like('msisdn', $msisdn);
  		$this->db->from('SMSC_dnd');
                $query = $this->db->limit(20);
  		$query = $this->db->get();
        return $query->result_array();
    }

    public function search()
    {
        $network_id = (int) $this->input->post('network_id');

        $msisdn_array = explode(",", $this->input->post('msisdn')); // get msisdn into array with spaces
        $msisdn = implode(",", array_map("trim", $msisdn_array));   // trim spaces 
        $split_commas_newlines = preg_split('/[\ \n\,]+/', $msisdn);        // split commas and newlines into array
        $msisdn = implode(",", array_map("trim", $split_commas_newlines));   // trim spaces 
        $msisdn_array = explode(",", $msisdn); // put back into array
        $count = count($split_commas_newlines);
        $where = '';
        //var_dump($msisdn); die();

        //$msisdn_array = explode(",", $this->input->post('msisdn'));
        //$msisdn_array = array_map("trim", $msisdn_array);

        $msisdn_first_item = array_shift($msisdn_array); // get first msisdn from array and reove it from array
        $msisdn_first_item = filter_var($msisdn_first_item, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        if($network_id == 0)
        {
            $where = "(SELECT *, SMSC_dnd.id AS id, networks.id AS network_id, networks.title_short AS network_title_short FROM `SMSC_dnd` LEFT OUTER JOIN `networks` ON SMSC_dnd.network_id = networks.id WHERE msisdn='". $msisdn_first_item . "' LIMIT 1 )";
        }
        else
        {
            $where = "(SELECT *, SMSC_dnd.id AS id, networks.id AS network_id, networks.title_short AS network_title_short FROM `SMSC_dnd` LEFT OUTER JOIN `networks` ON SMSC_dnd.network_id = networks.id WHERE (network_id = '". $network_id . "') AND (msisdn='". $msisdn_first_item . "'" . ") LIMIT 1)";
        }

        foreach ($msisdn_array as $msisdn) {
            //var_dump($msisdn_array); die();
            if((!empty($msisdn)) && (strlen($msisdn) > 5) )
            {
                $msisdn = filter_var($msisdn, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
                if($network_id == 0)
                {
                    $where =  $where . " UNION (SELECT *, SMSC_dnd.id AS id, networks.id AS network_id, networks.title_short AS network_title_short FROM `SMSC_dnd` LEFT OUTER JOIN `networks` ON SMSC_dnd.network_id = networks.id WHERE msisdn='". $msisdn . "' LIMIT 1)";
                }
                else
                {
                    $where =  $where . " UNION (SELECT *, SMSC_dnd.id AS id, networks.id AS network_id, networks.title_short AS network_title_short FROM `SMSC_dnd` LEFT OUTER JOIN `networks` ON SMSC_dnd.network_id = networks.id WHERE (network_id = '". $network_id . "') AND (msisdn='". $msisdn . "') LIMIT 1)";
                }
            }
        }
        $where .= ";";

        //var_dump($where); die();

        $query = $this->db->query($where);
        return $query->result_array();
    }

    public function add($csv)
    {
        $network_id = (int) $this->input->post('network_id');
        $network = '';

        switch ($network_id)
        {
            case 1:
                $network = 'MTN';
                break;
            case 2:
                $network = 'EMTS';
                break;
            case 3:
                $network = 'GLO';
                break;
            case 4:
                $network = 'AIRTEL';
                break;
            case 5:
                $network = 'NTEL';
                break;

            default:
                $network = '';
        }
        
        $date = date('Y-m-d H:i:s');
        $date_format = 'Y-m-d H:i:s';
        $date_format_post = $this->input->post('date_format');
        if($date_format_post == '2')
        {
            $date_format = 'd/m/Y H:i:s';
        }else if($date_format_post == '3')
        {
            $date_format = 'm/d/Y H:i:s';
        }

        $created_by = (int) $this->session->userdata('user_id');

        if($network == 'MTN')
        {
            foreach($csv as $csv_data)
            {
                //var_dump($csv_data); die();
                if((!empty($csv_data[0])) && (!empty($csv_data[1])) && (!empty($csv_data[2])) && (strtoupper(trim($csv_data[1])) != "OPERATE_DATE") )
                {
                    //date('Y-m-d H:i:s', strtotime($csv_data[1]) );
                    $csv_data[1] = trim($csv_data[1]);

                    $csv_data[1] = trim($csv_data[1]);

                    $tmp = explode(" ", $csv_data[1]); //print_r($tmp); die;
                    //$hr_min_sec = end($tmp);
                    $hr_min_sec = (count($tmp) == 1) ? "00:00:00" : end($tmp);  
                    $hr_min_sec = (strlen($hr_min_sec) == 4) || (strlen($hr_min_sec) == 5) ? $hr_min_sec . ":00" : $hr_min_sec;


                    $date1 = $tmp[0] . " " . $hr_min_sec;
                    //var_dump($date1);
                    //var_dump($date_format); die();

                    $out1 = DateTime::createFromFormat($date_format, trim($date1));
                    $csv_data[1] = $out1->format('Y-m-d H:i:s'); 
                    //$csv_data[1] = date('Y-m-d H:i:s', strtotime(trim($csv_data[1])));

                    //var_dump(date('Y-m-d H:i:s', strtotime(trim($csv_data[1])))); die();
                    $data = array(
                        'network_id' => $network_id,
                        'network' => $network,
                        'msisdn' => trim($csv_data[0]),
                        'operate_date' => trim($csv_data[1]),
                        'blocking_mode' => trim($csv_data[2]),
                        'banking_financial_services' => $this->bit(trim($csv_data[3])),
                        'real_estate' => $this->bit(trim($csv_data[4])),
                        'health' => $this->bit(trim($csv_data[5])),
                        'education' => $this->bit(trim($csv_data[6])),
                        'consumer_goods' => $this->bit(trim($csv_data[7])),
                        'broadcasting' => $this->bit(trim($csv_data[8])),
                        'tourism_leisure' => $this->bit(trim($csv_data[9])),
                        'sports' => $this->bit(trim($csv_data[10])),
                        'religion' => $this->bit(trim($csv_data[11])),
                        'subscription' => $this->bit(trim($csv_data[12])),
                        'broadcast' => $this->bit(trim($csv_data[13])),
                        'bulk_sms' => $this->bit(trim($csv_data[14])),
                        'bank_alerts' => $this->bit(trim($csv_data[15])),
                        'mtn_campaigns' => $this->bit(trim($csv_data[16])),
                        'created_by' => $created_by
                    );

                    // log msisdn bulk sms status
                    $status = $this->bit(trim($csv_data[14])) == "0" ? "add" : "remove";
                    $input_data = trim($csv_data[0]) . ',' . $status;
                    $handle = fopen("/home/emhollatags/RM/bulksms-dnd-msisdn-".date('Y-m-d').".csv", "a");
                    //$handle = fopen("../RM/bulksms-dnd-msisdn-".date('Y-m-d').".csv", "a");
                    fputcsv($handle, explode(',', $input_data));

                    $sql = 'INSERT INTO SMSC_dnd(network_id, network, msisdn, operate_date, blocking_mode, banking_financial_services, real_estate, health, education, consumer_goods, broadcasting, tourism_leisure, sports, religion, subscription, broadcast, bulk_sms, bank_alerts, mtn_campaigns, created_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE 
                    network_id=VALUES(network_id), 
                    network=VALUES(network), 
                    msisdn=VALUES(msisdn), 
                    operate_date=VALUES(operate_date), 
                    blocking_mode=VALUES(blocking_mode), 
                    banking_financial_services=VALUES(banking_financial_services), 
                    real_estate=VALUES(real_estate), 
                    health=VALUES(health), 
                    education=VALUES(education), 
                    consumer_goods=VALUES(consumer_goods), 
                    broadcasting=VALUES(broadcasting), 
                    tourism_leisure=VALUES(tourism_leisure), 
                    sports=VALUES(sports), 
                    religion=VALUES(religion), 
                    subscription=VALUES(subscription), 
                    broadcast=VALUES(broadcast), 
                    bulk_sms=VALUES(bulk_sms), 
                    bank_alerts=VALUES(bank_alerts), 
                    mtn_campaigns=VALUES(mtn_campaigns), 
                    no_of_duplicates=no_of_duplicates + 1, 
                    last_updated=NOW()';

                    $query = $this->db->query($sql, $data);
                    //return $this->db->insert('SMSC_dnd', $data);
                    //return $query;
                }
            }
        }
        elseif($network == 'GLO')
        {
            foreach($csv as $csv_data)
            {
                //var_dump($csv_data); die();
                if((!empty($csv_data[0])) && (strlen($csv_data[0]) > 6) )
                {
                    $msisdn = trim($csv_data[0]);

                    $country_code = '234';
                    $msisdn = trim($msisdn);
                    if (substr($msisdn, 0, 1) == '0'){
                        $msisdn = $country_code . substr($msisdn, 1);
                    }
                    elseif (substr($msisdn, 0, 1) == '+'){
                        $msisdn = substr($msisdn, 1);
                    }
                    elseif (substr($msisdn, 0, 3) == '234'){
                        // do nothing
                    }
                    else
                    {
                        //$msisdn = '0' .  $msisdn;
                        $msisdn = '234' .  $msisdn;
                    }
                    //var_dump($msisdn); die();

                    if(isset($csv_data[1]))
                    {
                        if(!empty($csv_data[1]))
                        {
                            $csv_data[1] = trim($csv_data[1]);

                            $csv_data[1] = trim($csv_data[1]);

                            $tmp = explode(" ", $csv_data[1]); //print_r($tmp); die;
                            //$hr_min_sec = end($tmp);
                            $hr_min_sec = (count($tmp) == 1) ? "00:00:00" : end($tmp);  
                            $hr_min_sec = (strlen($hr_min_sec) == 4) || (strlen($hr_min_sec) == 5) ? $hr_min_sec . ":00" : $hr_min_sec;


                            $date1 = $tmp[0] . " " . $hr_min_sec;
                            //var_dump($date1);
                            //var_dump($date_format); die();

                            $out1 = DateTime::createFromFormat($date_format, trim($date1));
                            $csv_data[1] = $out1->format('Y-m-d H:i:s'); 
                            $date = $csv_data[1];
                        }
                    }

                    $data = array(
                        'network_id' => $network_id,
                        'network' => $network,
                        'msisdn' => $msisdn,
                        'operate_date' => $date,
                        // 'operate_date' => trim($csv_data[1]),
                        'blocking_mode' => isset($csv_data[2]) ? trim($csv_data[2]) : 'Full',
                        'banking_financial_services' => isset($csv_data[3]) ? $this->bit(trim($csv_data[3])) : 0,
                        'real_estate' => isset($csv_data[4]) ? $this->bit(trim($csv_data[4])) : 0,
                        'health' => isset($csv_data[5]) ? $this->bit(trim($csv_data[5])) : 0,
                        'education' => isset($csv_data[6]) ? $this->bit(trim($csv_data[6])) : 0,
                        'consumer_goods' => isset($csv_data[7]) ? $this->bit(trim($csv_data[7])) : 0,
                        'broadcasting' => isset($csv_data[8]) ? $this->bit(trim($csv_data[8])) : 0,
                        'tourism_leisure' => isset($csv_data[9]) ? $this->bit(trim($csv_data[9])) : 0,
                        'sports' => isset($csv_data[10]) ? $this->bit(trim($csv_data[10])) : 0,
                        'religion' => isset($csv_data[11]) ? $this->bit(trim($csv_data[11])) : 0,
                        'subscription' => isset($csv_data[12]) ? $this->bit(trim($csv_data[12])) : 0,
                        'broadcast' => isset($csv_data[13]) ? $this->bit(trim($csv_data[13])) : 0,
                        'bulk_sms' => isset($csv_data[14]) ? $this->bit(trim($csv_data[14])) : 0,
                        'bank_alerts' => isset($csv_data[15]) ? $this->bit(trim($csv_data[15])) : 0,
                        'mtn_campaigns' => isset($csv_data[16]) ? $this->bit(trim($csv_data[16])) : 0,
                        'created_by' => $created_by
                    );

                    // log msisdn bulk sms status
                    $status = (isset($csv_data[14]) ? $this->bit(trim($csv_data[14])) : 0) == "0" ? "add" : "remove";
                    $input_data = trim($msisdn) . ',' . $status;
                    $handle = fopen("/home/emhollatags/GLO_DND/GLO-dnd-msisdn-".date('Y-m-d').".csv", "a");
                    //$handle = fopen("../RM/bulksms-dnd-msisdn-".date('Y-m-d').".csv", "a");
                    fputcsv($handle, explode(',', $input_data));

                    $sql = 'INSERT INTO SMSC_dnd(network_id, network, msisdn, operate_date, blocking_mode, banking_financial_services, real_estate, health, education, consumer_goods, broadcasting, tourism_leisure, sports, religion, subscription, broadcast, bulk_sms, bank_alerts, mtn_campaigns, created_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE 
                    network_id=VALUES(network_id), 
                    network=VALUES(network), 
                    msisdn=VALUES(msisdn), 
                    operate_date=VALUES(operate_date), 
                    blocking_mode=VALUES(blocking_mode), 
                    banking_financial_services=VALUES(banking_financial_services), 
                    real_estate=VALUES(real_estate), 
                    health=VALUES(health), 
                    education=VALUES(education), 
                    consumer_goods=VALUES(consumer_goods), 
                    broadcasting=VALUES(broadcasting), 
                    tourism_leisure=VALUES(tourism_leisure), 
                    sports=VALUES(sports), 
                    religion=VALUES(religion), 
                    subscription=VALUES(subscription), 
                    broadcast=VALUES(broadcast), 
                    bulk_sms=VALUES(bulk_sms), 
                    bank_alerts=VALUES(bank_alerts), 
                    mtn_campaigns=VALUES(mtn_campaigns), 
                    no_of_duplicates=no_of_duplicates + 1, 
                    last_updated=NOW()';

                    $query = $this->db->query($sql, $data);
                }
            }
        }
        else
        {
            foreach($csv as $csv_data)
            {
                //var_dump($csv_data); die();
                // if((!empty($csv_data[0])) && (!empty($csv_data[1])) && (!empty($csv_data[2])) && (strtoupper(trim($csv_data[1])) != "OPERATE_DATE") )
                if((!empty($csv_data[0])) && (strlen($csv_data[0]) > 6) )
                {
                    $msisdn = trim($csv_data[0]);

                    $country_code = '234';
                    $msisdn = trim($msisdn);
                    if (substr($msisdn, 0, 1) == '0'){
                        $msisdn = $country_code . substr($msisdn, 1);
                    }
                    elseif (substr($msisdn, 0, 1) == '+'){
                        $msisdn = substr($msisdn, 1);
                    }
                    elseif (substr($msisdn, 0, 3) == '234'){
                        // do nothing
                    }
                    else
                    {
                        //$msisdn = '0' .  $msisdn;
                        $msisdn = '234' .  $msisdn;
                    }

                    //date('Y-m-d H:i:s', strtotime($csv_data[1]) );

                    if(isset($csv_data[1]))
                    {
                        if(!empty($csv_data[1]))
                        {
                            $csv_data[1] = trim($csv_data[1]);

                            $csv_data[1] = trim($csv_data[1]);

                            $tmp = explode(" ", $csv_data[1]); //print_r($tmp); die;
                            //$hr_min_sec = end($tmp);
                            $hr_min_sec = (count($tmp) == 1) ? "00:00:00" : end($tmp);  
                            $hr_min_sec = (strlen($hr_min_sec) == 4) || (strlen($hr_min_sec) == 5) ? $hr_min_sec . ":00" : $hr_min_sec;


                            $date1 = $tmp[0] . " " . $hr_min_sec;
                            //var_dump($date1);
                            //var_dump($date_format); die();

                            $out1 = DateTime::createFromFormat($date_format, trim($date1));
                            $csv_data[1] = $out1->format('Y-m-d H:i:s'); 
                            $date = $csv_data[1];
                        }
                    }
                    //$csv_data[1] = date('Y-m-d H:i:s', strtotime(trim($csv_data[1])));

                    //var_dump(date('Y-m-d H:i:s', strtotime(trim($csv_data[1])))); die();
                    $data = array(
                        'network_id' => $network_id,
                        'network' => $network,
                        'msisdn' => $msisdn,
                        'operate_date' => $date,
                        'blocking_mode' => isset($csv_data[2]) ? trim($csv_data[2]) : 'Full',
                        'banking_financial_services' => isset($csv_data[3]) ? $this->bit(trim($csv_data[3])) : 0,
                        'real_estate' => isset($csv_data[4]) ? $this->bit(trim($csv_data[4])) : 0,
                        'health' => isset($csv_data[5]) ? $this->bit(trim($csv_data[5])) : 0,
                        'education' => isset($csv_data[6]) ? $this->bit(trim($csv_data[6])) : 0,
                        'consumer_goods' => isset($csv_data[7]) ? $this->bit(trim($csv_data[7])) : 0,
                        'broadcasting' => isset($csv_data[8]) ? $this->bit(trim($csv_data[8])) : 0,
                        'tourism_leisure' => isset($csv_data[9]) ? $this->bit(trim($csv_data[9])) : 0,
                        'sports' => isset($csv_data[10]) ? $this->bit(trim($csv_data[10])) : 0,
                        'religion' => isset($csv_data[11]) ? $this->bit(trim($csv_data[11])) : 0,
                        'subscription' => isset($csv_data[12]) ? $this->bit(trim($csv_data[12])) : 0,
                        'broadcast' => isset($csv_data[13]) ? $this->bit(trim($csv_data[13])) : 0,
                        'bulk_sms' => isset($csv_data[14]) ? $this->bit(trim($csv_data[14])) : 0,
                        'bank_alerts' => isset($csv_data[15]) ? $this->bit(trim($csv_data[15])) : 0,
                        'mtn_campaigns' => isset($csv_data[16]) ? $this->bit(trim($csv_data[16])) : 0,
                        'created_by' => $created_by
                    );

                    // var_dump($data); die();

                    /*
                    // log msisdn bulk sms status
                    $status = bit(trim($csv_data[14])) == "0" ? "add" : "remove";
                    $input_data = trim($csv_data[0]) . ',' . $status;
                    $handle = fopen("/home/emhollatags/RM/bulksms-dnd-msisdn-".date('Y-m-d').".csv", "a");
                    //$handle = fopen("../RM/bulksms-dnd-msisdn-".date('Y-m-d').".csv", "a");
                    fputcsv($handle, explode(',', $input_data));
                    */

                    $sql = 'INSERT INTO SMSC_dnd(network_id, network, msisdn, operate_date, blocking_mode, banking_financial_services, real_estate, health, education, consumer_goods, broadcasting, tourism_leisure, sports, religion, subscription, broadcast, bulk_sms, bank_alerts, mtn_campaigns, created_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    ON DUPLICATE KEY UPDATE 
                    network_id=VALUES(network_id), 
                    network=VALUES(network), 
                    msisdn=VALUES(msisdn), 
                    operate_date=VALUES(operate_date), 
                    blocking_mode=VALUES(blocking_mode), 
                    banking_financial_services=VALUES(banking_financial_services), 
                    real_estate=VALUES(real_estate), 
                    health=VALUES(health), 
                    education=VALUES(education), 
                    consumer_goods=VALUES(consumer_goods), 
                    broadcasting=VALUES(broadcasting), 
                    tourism_leisure=VALUES(tourism_leisure), 
                    sports=VALUES(sports), 
                    religion=VALUES(religion), 
                    subscription=VALUES(subscription), 
                    broadcast=VALUES(broadcast), 
                    bulk_sms=VALUES(bulk_sms), 
                    bank_alerts=VALUES(bank_alerts), 
                    mtn_campaigns=VALUES(mtn_campaigns), 
                    no_of_duplicates=no_of_duplicates + 1, 
                    last_updated=NOW()';

                    $query = $this->db->query($sql, $data);
                    //return $this->db->insert('SMSC_dnd', $data);
                    //return $query;
                }
            }
        }

        $sql = 'UPDATE summary SET total = (SELECT COUNT(id) FROM `SMSC_dnd`), today = (SELECT COUNT(id) FROM `SMSC_dnd` WHERE DATE(`date_created`) = CURDATE()) WHERE category = "dnd"';
        $query = $this->db->query($sql);
    }

    private function bit($value)
    {
        if($value != '0')
        {
            return '1';
        }
        else
        {
            return '0';
        }
    }
}