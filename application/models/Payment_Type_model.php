<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Payment_Type_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function filter_record_count($id = FALSE, $name = FALSE)
    {
        $this->db->reset_query();

        $sql = "SELECT COUNT(id) AS count FROM payment_types WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (id = '". $id . "')";
            }
        }

        if($name !== FALSE)
        {
            $name_full = '%' . filter_var($name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            // $where .= "(name LIKE '". $name_full . "') OR (code LIKE '". $name_full . "')";
            $where .= "(name LIKE '". $name_full . "')";

            $name_array = explode(' ', $name);
            if(count($name_array) > 1)
            {
                foreach ($name_array as $name_word) {
                    if(!empty($name_word))
                    {
                        $name_word = '%' . filter_var($name_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        // $where .= " OR (name LIKE '". $name_word . "') OR (code LIKE '". $name_word . "')";
                        $where .= " OR (name LIKE '". $name_word . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $id = FALSE, $name = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $sql = "SELECT id, name FROM payment_types WHERE ";

        $where = '';

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (id = '". $id . "')";
            }
        }

        if($name !== FALSE)
        {
            $name_full = '%' . filter_var($name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
            $where .= " AND ("; 

            // $where .= "(name LIKE '". $name_full . "') OR (code LIKE '". $name_full . "')";
            $where .= "(name LIKE '". $name_full . "')";

            $name_array = explode(' ', $name);
            if(count($name_array) > 1)
            {
                foreach ($name_array as $name_word) {
                    if(!empty($name_word))
                    {
                        $name_word = '%' . filter_var($name_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                        // $where .= " OR (name LIKE '". $name_word . "') OR (code LIKE '". $name_word . "')";
                        $where .= "(name LIKE '". $name_full . "')";
                    }
                }
            }

            $where .= ")";
        }
        
        $where = ltrim($where, ' AND');
        $sql = $sql . $where . " ORDER BY name ASC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count() {
        return $this->db->count_all("payment_types");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name');
            $this->db->from('payment_types'); 
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();
            return $query->result_array();
        }

        $this->db->select('id, name');
        $this->db->from('payment_types');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getRowsDropDown($id = FALSE)
    {
        if ($id === FALSE)
        {
            $this->db->order_by('name', 'ASC');
            $this->db->select('id, name');
            $this->db->from('payment_types'); 
            $query = $this->db->get();
            
            return $query->result_array();
        }
        $this->db->select('name');
        $this->db->from('payment_types');
        $this->db->where('id', $id); 
        $query = $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function add()
    {
        $created_by = (int) $this->session->userdata('user_id');

        $data = array(
            'name' => trim($this->input->post('name')),
            'created_by' => $created_by
        );

        $this->db->insert('payment_types', $data);
    }

    public function delete($id)
    {
        $this->db->delete('payment_types', array('id' => (int) $id));
    }

    public function update($id)
    {

        $data = array(
            'name' => trim($this->input->post('name'))
        );
        $this->db->where('id', (int) $id);
        $this->db->update('payment_types', $data);
    }
}