<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function countPaidRevenueTotal($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "customer")
            {
                $sql = "SELECT SUM(total_price) AS total FROM `orders` WHERE status_id != '4' AND user_id = '$user_id'";
            }

            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['total'];
        }

        $sql = "SELECT SUM(total_price) AS total FROM `orders` WHERE status_id != '4'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['total'];
    }

    public function countPaid($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;

            if($type == "customer")
            {
                $sql = "SELECT COUNT(id) AS count FROM `orders` WHERE status_id != '4' AND user_id = '$user_id'";
            }

            $query = $this->db->query($sql);
            $row = $query->row_array();
            return $row['count'];
        }

        $sql = "SELECT COUNT(id) AS count FROM `orders` WHERE status_id != '4'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row['count'];
    }

    public function filter_record_count($user_id = FALSE, $id = FALSE, $title = FALSE, $status_id = FALSE, $date = FALSE)
    {

        $sql = "SELECT COUNT(DISTINCT(orders.id)) AS count FROM orders LEFT JOIN order_details ON orders.id = order_details.order_id LEFT JOIN payments ON orders.id = payments.order_id LEFT JOIN users ON orders.user_id = users.id WHERE ";

        $where = '';

        if($user_id !== FALSE)
        {
            $user_id = (int) $user_id;
            if($user_id != 0)
            {
                $where .= " AND (orders.user_id = '". $user_id . "')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (orders.id = '". $id . "')";
            }
        }

        if($status_id !== FALSE)
        {
            $status_id = (int) $status_id;
            if($status_id != -1)
            {
                $where .= " AND (orders.status_id = '". $status_id . "')";
            }
        }

        if($date !== FALSE)
        {
            $where .= " AND (DATE(orders.date_created) = '". $date . "')";
        }

        if($title !== FALSE)
        {
            if(!empty($title))
            {
                // $title = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                // $where .= " AND ((users.full_name LIKE '". $title . "') OR (orders.items LIKE '". $title . "'))";

                $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                $where .= " AND ("; 

                $where .= "(users.full_name LIKE '". $title_full . "') OR (products.name LIKE '". $title_full . "')";

                $title_array = explode(' ', $title);
                if(count($title_array) > 1)
                {
                    foreach ($title_array as $title_word) {
                        if(!empty($title_word))
                        {
                            $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                            $where .= " OR (users.full_name LIKE '". $title_word . "') OR (products.name LIKE '". $title_word . "')";
                        }
                    }
                }

                $where .= ")";
            }
        }

        if(empty($where))
        {
            // all null, remove where
            $sql = rtrim($sql, ' WHERE');
        }
        
        // echo $where; die;
        $where = ltrim($where, ' AND');
        // $where .= " GROUP BY orders.id, orders.user_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.total_price, orders.status_id, orders.date_created, payments.successful, payments.error_code, payments.error_message, users.full_name ";
        $sql = $sql . $where;

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->row_array()['count'];
    }

    public function filter($limit, $offset, $user_id = FALSE, $id = FALSE, $title = FALSE, $status_id = FALSE, $date = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;


        $sql = "SELECT orders.id, orders.user_id, orders.total_price, orders.payment_type_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.status_id, orders.date_created, status.name AS status, payment_types.name AS payment_type, status.description, GROUP_CONCAT(DISTINCT CONCAT(order_details.product_id, '@@@', order_details.quantity, '@@@', products.name, '@@@', products.price, '@@@', products.image_url) ORDER BY orders.id SEPARATOR '###') AS items, payments.successful, payments.error_code, payments.error_message, users.full_name FROM orders LEFT JOIN order_details ON orders.id = order_details.order_id LEFT JOIN payments ON orders.id = payments.order_id LEFT JOIN products ON order_details.product_id = products.id LEFT JOIN status ON orders.status_id = status.id LEFT JOIN payment_types ON orders.payment_type_id = payment_types.id LEFT JOIN users ON orders.user_id = users.id WHERE ";

        $where = '';

        if($user_id !== FALSE)
        {

            $user_id = (int) $user_id;
            if($user_id != 0)
            {
                $where .= " AND (orders.user_id = '". $user_id . "')";
            }
        }

        if($id !== FALSE)
        {

            $id = (int) $id;
            if($id != 0)
            {
                $where .= " AND (orders.id = '". $id . "')";
            }
        }

        if($status_id !== FALSE)
        {
            $status_id = (int) $status_id;
            if($status_id != -1)
            {
                $where .= " AND (orders.status_id = '". $status_id . "')";
            }
        }

        if($date !== FALSE)
        {
            $where .= " AND (DATE(orders.date_created) = '". $date . "')";
        }

        if($title !== FALSE)
        {
            if(!empty($title))
            {
                // $title = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                // $where .= " AND ((users.full_name LIKE '". $title . "') OR (orders.items LIKE '". $title . "'))";

                $title_full = '%' . filter_var($title, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                $where .= " AND ("; 

                $where .= "(users.full_name LIKE '". $title_full . "') OR (products.name LIKE '". $title_full . "')";

                $title_array = explode(' ', $title);
                if(count($title_array) > 1)
                {
                    foreach ($title_array as $title_word) {
                        if(!empty($title_word))
                        {
                            $title_word = '%' . filter_var($title_word, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH) . '%';
                            $where .= " OR (users.full_name LIKE '". $title_word . "') OR (products.name LIKE '". $title_word . "')";
                        }
                    }
                }

                $where .= ")";
            }
        }
        
        if(empty($where))
        {
            // all null, remove where
            $sql = rtrim($sql, ' WHERE');
        }
        
        $where = ltrim($where, ' AND');
        $where .= " GROUP BY orders.id, orders.user_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.total_price, orders.status_id, orders.date_created, payments.successful, payments.error_code, payments.error_message, users.full_name ";
        $sql = $sql . $where . " ORDER BY orders.date_created DESC LIMIT $offset, $limit";

        // echo $sql; die;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function record_count($type = "all", $user_id = FALSE)
    {
        if($user_id !== FALSE)
        {
            if($type == "customer")
            {
                $this->db->where('user_id', (int) $user_id);
            }
            $this->db->from('orders');
            return $this->db->count_all_results();
        }
        return $this->db->count_all("orders");
    }

    public function getRows($limit, $offset, $id = FALSE)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        if($id === FALSE)
        {
            $limit = (int) $limit;
            $offset = (int) $offset;
            
// orders.id, orders.user_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.total_price, orders.status_id, orders.date_created, GROUP_CONCAT(DISTINCT CONCAT(order_details.product_id, '@@@', products.name, '@@@', products.price) ORDER BY order_details.order_id SEPARATOR '###') AS items, payments.successful, payments.error_code, payments.error_message, users.full_name FROM orders LEFT JOIN order_details ON orders.id = order_details.order_id LEFT JOIN payments ON orders.id = payments.order_id LEFT JOIN users ON orders.user_id = users.id

            $this->db->order_by('orders.date_created', 'DESC');
            $this->db->select("orders.id, orders.user_id, orders.total_price, orders.payment_type_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.status_id, orders.date_created, status.name AS status, payment_types.name AS payment_type, status.description, GROUP_CONCAT(DISTINCT CONCAT(order_details.product_id,'@@@',order_details.quantity,'@@@',products.name,'@@@',products.price,'@@@',products.image_url) ORDER BY orders.id SEPARATOR '###') AS items, payments.successful, payments.error_code, payments.error_message, users.full_name");
            $this->db->from('orders'); 
        $this->db->join('order_details', 'orders.id = order_details.order_id', 'left');
        $this->db->join('payments', 'orders.id = payments.order_id', 'left');
        $this->db->join('products', 'order_details.product_id = products.id', 'left');
        $this->db->join('status', 'orders.status_id = status.id', 'left');
        $this->db->join('payment_types', 'orders.payment_type_id = payment_types.id', 'left');
        $this->db->join('users', 'orders.user_id = users.id', 'left');
            $this->db->group_by('orders.id, orders.user_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.total_price, orders.status_id, orders.date_created, payments.successful, payments.error_code, payments.error_message');
            $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

            $query = $this->db->get();  // echo $this->db->last_query(); die;
            return $query->result_array();
        }

        $this->db->order_by('orders.date_created', 'DESC');
        $this->db->select("orders.id, orders.user_id, orders.total_price, orders.payment_type_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.status_id, orders.date_created, status.name AS status, payment_types.name AS payment_type, status.description, GROUP_CONCAT(DISTINCT CONCAT(order_details.product_id,'@@@',order_details.quantity,'@@@',products.name,'@@@',products.price,'@@@',products.image_url) ORDER BY orders.id SEPARATOR '###') AS items, payments.successful, payments.error_code, payments.error_message, users.full_name");
        $this->db->from('orders'); 
        $this->db->join('order_details', 'orders.id = order_details.order_id', 'left');
        $this->db->join('payments', 'orders.id = payments.order_id', 'left');
        $this->db->join('products', 'order_details.product_id = products.id', 'left');
        $this->db->join('status', 'orders.status_id = status.id', 'left');
        $this->db->join('payment_types', 'orders.payment_type_id = payment_types.id', 'left');
        $this->db->join('users', 'orders.user_id = users.id', 'left');
        $this->db->where('orders.id', (int) $id); 
        $this->db->group_by('orders.id, orders.user_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.total_price, orders.status_id, orders.date_created, payments.successful, payments.error_code, payments.error_message');
        $this->db->limit(1);

        $query = $this->db->get(); // echo $this->db->last_query(); die;
        return $query->row_array();
    }

    public function getRowsByUserID($limit, $offset, $user_id)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $this->db->order_by('orders.date_created', 'DESC');
        $this->db->select("orders.id, orders.user_id, orders.total_price, orders.payment_type_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.status_id, orders.date_created, status.name AS status, payment_types.name AS payment_type, status.description, GROUP_CONCAT(DISTINCT CONCAT(order_details.product_id,'@@@',order_details.quantity,'@@@',products.name,'@@@',products.price,'@@@',products.image_url) ORDER BY orders.id SEPARATOR '###') AS items, payments.successful, payments.error_code, payments.error_message");
        $this->db->from('orders'); 
        $this->db->join('order_details', 'orders.id = order_details.order_id', 'left');
        $this->db->join('payments', 'orders.id = payments.order_id', 'left');
        $this->db->join('products', 'order_details.product_id = products.id', 'left');
        $this->db->join('status', 'orders.status_id = status.id', 'left');
        $this->db->join('payment_types', 'orders.payment_type_id = payment_types.id', 'left');
        $this->db->where('orders.user_id', (int) $user_id); 
        $this->db->group_by('orders.id, orders.user_id, orders.order_comment, orders.delivery_country_id, orders.delivery_address, orders.total_price, orders.status_id, orders.date_created, payments.successful, payments.error_code, payments.error_message');
        $this->db->limit($limit, $offset); // produces LIMIT $offset, $limit

        $query = $this->db->get(); // echo $this->db->last_query(); die;
        return $query->result_array();
    }

    public function getOrderItemsByUserID($user_id, $id)
    {
        $user_id = (int) $user_id;
        $id = (int) $id;

        $this->db->order_by('orders.date_created', 'DESC');
        $this->db->select("orders.id, order_details.product_id, GROUP_CONCAT(DISTINCT CONCAT(past_exam_solutions.file_url) ORDER BY order_details.order_id SEPARATOR ',') AS file_url");
        $this->db->from('orders'); 
        $this->db->join('order_details', 'orders.id = order_details.order_id', 'left');
        $this->db->join('past_exam_solutions', 'order_details.product_id = past_exam_solutions.id', 'left');
        $this->db->where('orders.user_id', $user_id); 
        $this->db->where('order_details.order_id', $id); 
        $this->db->where('orders.status_id', 1); 
        $this->db->group_by('order_details.product_id');  

        $query = $this->db->get(); // echo $this->db->last_query(); die;
        return $query->result_array();
    }

    public function getRecentItems($limit)
    {
        $limit = (int) $limit;

        $this->db->order_by('date_created', 'DESC');
        $this->db->select('id, title, price');
        $this->db->from('orders');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function add($total_price, $payment_type_id, $delivery_country_id, $delivery_address)
    {
        $user_id = (int) $this->session->userdata('user_id');

        $data = array(
            'user_id' => $user_id,
            'total_price' => (float) $total_price,
            'payment_type_id' => (int) trim($payment_type_id),
            'delivery_country_id' => trim($delivery_country_id),
            'delivery_address' => trim($delivery_address),
            'status_id' => 1
        );

        $this->db->insert('orders', $data);
        
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function paid($id)
    {
        $data = array(
            'status_id' => 1
        );
        $this->db->where('id', (int) $id);
        $this->db->update('orders', $data);
    }

    public function addDetails($order_id, $quantity, $product_id)
    {

        $data = array(
            'order_id' => (int) $order_id,
            'quantity' => (int) $quantity,
            'product_id' => (int) $product_id
        );

        $this->db->insert('order_details', $data);
    }

    public function addItems($order_id, $item_id, $item_name, $item_price)
    {

        $data = array(
            'order_id' => (int) $order_id,
            'item_id' => (int) $item_id,
            'item_name' => trim($item_name),
            'item_price' => (float) $item_price
        );

        $this->db->insert('order_details', $data);
    }

    public function delete($id)
    {
        $this->db->delete('orders', array('id' => (int) $id));
    }

    public function updateOrderStatus($id, $status_id)
    {
        $data = array(
            'status_id' => (int) $status_id
        );
        $this->db->where('id', (int) $id);
        $this->db->update('orders', $data);
    }
}