<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('User_model');
            $this->load->library("pagination");
    }

	public function index()
	{
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Students';

        // Pagination
        $id = isset($_GET['id']) ? trim($_GET['id']) : FALSE;
        $title = isset($_GET['title']) ? trim($_GET['title']) : FALSE;

        if($id || $title)
        {
            $config["total_rows"] = $this->User_model->filter_record_count("student", $id, $title);
        }
        else
        {
            $config["total_rows"] = $this->User_model->record_count("student");
        }



        $config["base_url"] = base_url() . "admin123/students/index";
        // $config["total_rows"] = $this->User_model->record_count("student");
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($id || $title)
        {
            $data['rows'] = $this->User_model->filter($config["per_page"], $offset, "student", $id, $title);
        }
        else
        {
            $data['rows'] = $this->User_model->getRowsStudent($config["per_page"], $offset);
        }
        // $data["rows"] = $this->User_model->getRowsStudent($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

     $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
     $this->load->view($this->config->item('template_dir_admin') . 'menu');
     $this->load->view($this->config->item('template_dir_admin') . 'students', $data);  // load content view
	}

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'User Details';
        $data['row'] = $this->User_model->getRows($id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'user-details', $data);  // load content view
    }

    public function delete($id)
    {
        $data['row'] = $this->User_model->delete($id);
        redirect('/admin123/students', 'refresh');
    }

    public function switchto($id) // login to user account
    {
        $id = (int) $id;
        $data['row'] = $this->User_model->loginAs($id);
        if($data['row'])
        {
            redirect('/User-Home', 'refresh');
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to grant access. Please try again.");
            redirect('/admin123/students', 'refresh');
        }
    }

    public function suspend($id)
    {
        $data['row'] = $this->User_model->setSuspend($id, 1);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Student suspended successfully!");

        // // send notification to agent dashboard
        // $notification_user_id = $id;

        // $notification_message = 'Account suspended! Please contact support to resolve.';
        // add_notification($notification_user_id, $notification_message);
        // // -----------------------------------------------------------------

        redirect('/admin123/students', 'refresh');
    }

    public function unsuspend($id)
    {
        $data['row'] = $this->User_model->setSuspend($id, 0);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Student unsuspended successfully!");

        // // send notification to agent dashboard
        // $notification_user_id = $id;

        // $notification_message = 'Account unsuspended! Thank you for choosing US Uber.';
        // add_notification($notification_user_id, $notification_message);
        // // -----------------------------------------------------------------

        redirect('/admin123/students', 'refresh');
    }
}
