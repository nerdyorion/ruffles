<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zzz extends Admin_Controller {
    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->model('DND_Number_model');
            $this->load->model('MNP_Number_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
        $header['page_title'] = 'Home';
        $data['dnd_today'] = $this->DND_Number_model->getCountOld("TODAY");
        $data['dnd_total'] = $this->DND_Number_model->getCountOld("ALL");

        $data['mnp_today'] = $this->MNP_Number_model->getCountOld("TODAY");
        $data['mnp_total'] = $this->MNP_Number_model->getCountOld("ALL");

        $data['last_login'] = $this->session->userdata("user_last_login");
        //$data['rows'] = 'rows';
        $this->load->view('header', $header);  // load header view
        $this->load->view('menu');  // load menu view
        $this->load->view('home', $data);  // load content view
	}
}
