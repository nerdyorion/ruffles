<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Live_Chat extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
    }

	public function index()
	{
        $header['page_title'] = 'Live Chat';
        $data['row'] = '';

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'live-chat', $data);  // load content view
	}
}