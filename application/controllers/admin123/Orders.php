<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            if(!is_super_admin())  // check if is super admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
            $this->load->model('Order_model');
            $this->load->model('Order_Detail_model');
            $this->load->model('Payment_model');
            $this->load->model('Status_model');
            $this->load->library("pagination");
        }

        public function index()
        {
            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Orders';

            $data['status_all'] = $this->Status_model->getRowsDropDown();

        // Pagination
            $id = isset($_GET['id']) ? trim($_GET['id']) : FALSE;
            $title = isset($_GET['title']) ? trim($_GET['title']) : FALSE;
            $status_id = isset($_GET['status_id']) ? trim($_GET['status_id']) : FALSE;
            $date = isset($_GET['date']) ? trim($_GET['date']) : FALSE;

        // same as false conditions
            if($status_id == "-1")
            {
                $status_id = FALSE;
            }
            if(empty($title))
            {
                $title = FALSE;
            }
            if(empty($date))
            {
                $date = FALSE;
            }

        // if($country_id)
        // {
        //     $data['universities_all'] = $this->University_model->getRowsByCountryIDDropDown($country_id);
        // }

            if($id || $title || ($status_id != '-1' && $status_id != '')  || $date)
            {
                $config["total_rows"] = $this->Order_model->filter_record_count(FALSE, $id, $title, $status_id, $date);
            }
            else
            {
                $config["total_rows"] = $this->Order_model->record_count("all");
            }


            $config["base_url"] = base_url() . "admin123/orders/index";
        // $config["total_rows"] = $this->Order_model->record_count("all");
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            $config['use_page_numbers']  = TRUE;
            $config['reuse_query_string']=TRUE;

            $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
            $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li><span><b>";
            $config['cur_tag_close'] = "</b></span></li>";

            $this->pagination->initialize($config);

            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $offset = 0;
            if($page > 1)
            {
                $offset = ($page * $config["per_page"]) - $config["per_page"];
            }

            $data['sn'] = $offset == 0 ? 1 : $offset + 1;
            if($id || $title || ($status_id != '-1' && $status_id != '') || $date)
            {
                $data['rows'] = $this->Order_model->filter($config["per_page"], $offset, FALSE, $id, $title, $status_id, $date);
            }
            else
            {
                $data['rows'] = $this->Order_model->getRows($config["per_page"], $offset);
            }
        // $data["rows"] = $this->Order_model->getRows($config["per_page"], $offset);
            $data["links"] = $this->pagination->create_links();

            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
            $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'orders', $data);  // load content view
    }

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Order Details';
        $data['row'] = $this->Order_model->getRows(0, 0, $id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'order-details', $data);  // load content view
    }

    public function shipped($id)
    {
        $this->Order_model->updateOrderStatus($id, 2);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Order shipped successfully!");

        redirect("/admin123/orders");
    }

    public function delivered($id)
    {
        $this->Order_model->updateOrderStatus($id, 3);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Order delivered successfully!");

        redirect("/admin123/orders");
    }

    public function cancel($id)
    {
        $this->Order_model->updateOrderStatus($id, 4);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Order cancelled successfully!");

        redirect("/admin123/orders");
    }

    public function delete($id)
    {
        // delete order
        $this->Order_model->delete($id);

        // delete order items
        $this->Order_Detail_model->deleteByOrderID($id);

        // delete order payment record
        $this->Payment_model->deleteByOrderID($id);

        redirect('/admin123/orders', 'refresh');
    }
}
