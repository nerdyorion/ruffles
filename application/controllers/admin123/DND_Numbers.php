<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//ini_set('max_execution_time', 3600); //3600 seconds = 60 minutes
 set_time_limit(0); 

class DND_Numbers extends Admin_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('DND_Number_model');
            //$this->load->helper('url_helper');
    }

	public function index()
	{
        if(is_dnd_search_admin())  // check if is dnd_search admin
        {
            //redirect to 404
            redirect('/admin123/404');
            //show_404();
        }
		$data['rows'] = $this->DND_Number_model->getRows();
		$data['error'] = $this->session->flashdata('error');
		$data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Upload CSV';
        //$data['rows'] = 'rows';
        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'dnd-numbers', $data);  // load content view
	}

	public function create()
    {
            if(is_dnd_search_admin())  // check if is dnd_search admin
            {
                //redirect to 404
                redirect('/admin123/404');
                //show_404();
            }
        $config['upload_path']          = './assets/csv/dnd/';
        $config['allowed_types']        = 'csv|txt';
        $config['max_size']             = 51200;
        $new_name = "";
        $user_id = $this->session->userdata("user_id");

        if(isset($_FILES["csv_file"]['name']))
        {
            $file_name = $_FILES["csv_file"]['name'];
            $tmp = explode(".", $file_name);
            $file_ext = end($tmp);

            array_pop($tmp);
            $new_name = implode('', $tmp);

            $config['file_name'] = $new_name . "_$user_id." . $file_ext;
        }

        $this->load->library('upload', $config);

        if ((is_null($this->input->post('network_id'))) || ($this->input->post('network_id') == 0))
        {
            $errors = "Please select network!";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', 1);
            redirect("DND-Numbers");
        }
        else if ( !$this->upload->do_upload('csv_file'))
        {
            $errors = str_replace("<p>","", $this->upload->display_errors());
            $errors = str_replace("</p>","", $errors);
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', 1);
            redirect("/admin123/DND-Numbers");
        }
        else
        {
        	/*
        		$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
				if(in_array($_FILES['csv_file']['type'], $mimes)){
  					// do something
  					$csv = array_map('str_getcsv', file($_FILES['csv_file']['tmp']));
  					var_dump($csv);
				} 
					else {
        			$this->session->set_flashdata('error', "Sorry, mime type not allowed. Only valid csv file is allowed.");
        			redirect("DND-Numbers");
				}
			*/
			$csv = array_map('str_getcsv', file($_FILES['csv_file']['tmp_name']));
			//echo empty($csv[2][0]) ? 'empty' : "not empty";
  			//var_dump($csv);
  			//var_dump($this->upload->data());
  			$this->DND_Number_model->add($csv);
        	$this->session->set_flashdata('error', 'Record(s) added successfully.');
        	$this->session->set_flashdata('error_code', 0);
        	redirect("/admin123/DND-Numbers");
        }
    }

	public function search()
	{
    	$this->load->library('form_validation');
        $this->form_validation->set_rules('msisdn', 'MSISDN', 'trim|required|max_length[5000]');
        //$this->form_validation->set_rules('network_id', 'Network', 'trim|required|max_length[10]');

    	if ($this->form_validation->run() === FALSE)
    	{
        	$errors = str_replace("<p>","", validation_errors());
        	$errors = str_replace("</p>","", trim($errors));
        	$this->session->set_flashdata('error', $errors);
        	$this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
        	$header['page_title'] = 'Search DND Number';
        	$data['rows'] = '';
        	
        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        	$this->load->view($this->config->item('template_dir_admin') . 'dnd-numbers-search', $data);  // load content view
        }
    	else
    	{
			$data['error'] = $this->session->flashdata('error');
			$data['error_code'] = $this->session->flashdata('error_code');
    	                $header['page_title'] = 'Search DND Number';
			//$data['rows'] = $this->DND_Number_model->getRowByMSISDN();
			$data['rows'] = $this->DND_Number_model->search();

        	$this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        	$this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
    	    $this->load->view($this->config->item('template_dir_admin') . 'dnd-numbers-search', $data);
    	}
	}

	public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'DND Number Details';
        $data['row'] = $this->DND_Number_model->getRows($id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'dnd-number-details', $data);  // load content view
    }
}
