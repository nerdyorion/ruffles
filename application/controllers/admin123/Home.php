<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct()
    {
        // var_dump("hello"); die;
            parent::__construct();
            // $this->load->library('session');
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('Order_model');
            $this->load->model('User_model');
            $this->load->model('Product_model');
    }

	public function index()
	{
        $header['page_title'] = 'Home';
        $data['sn'] = 1;
        $data['orders_paid'] = (int) $this->Order_model->countPaid('all');
        $data['orders_paid_revenue_total'] = (float) $this->Order_model->countPaidRevenueTotal('all');
        // $data['categories_total'] = (int) $this->Category_model->record_count();
        $data['customers_total'] = (int) $this->User_model->record_count('customer');
        $data['products_total'] = (int) $this->Product_model->record_count();

        $data['last_login'] = $this->session->userdata("user_last_login");
        // $data["rows"] = $this->Past_Exam_Solution_model->getRowsAllUnpaid(5, 0);
        $data["rows"] = array();
        //$data['rows'] = 'rows';
        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'home', $data);  // load content view
	}
}