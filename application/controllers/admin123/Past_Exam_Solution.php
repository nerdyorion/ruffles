<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Past_Exam_Solution extends Admin_Controller {

    // private $upload_save_path = '/var/www/html/academicianhelp-past-exam-solution-files/';
    private $upload_save_path = '/home/brilll8h/public_html/academicianhelp-past-exam-solution-files/';

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/admin123/login');
            }
            $this->load->model('University_model');
            $this->load->model('Past_Exam_Solution_model');
            $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'Past Exam Solution';

        $data['universities'] = $this->University_model->getRowsDropDown();
        $data['universities_all'] = $data['universities'];

        // Pagination
        $id = isset($_GET['id']) ? trim($_GET['id']) : FALSE;
        $university_id = isset($_GET['university_id']) ? trim($_GET['university_id']) : FALSE;
        $title = isset($_GET['title']) ? trim($_GET['title']) : FALSE;

        if($id || $university_id || $title)
        {
            $config["total_rows"] = $this->Past_Exam_Solution_model->filter_record_count($id, FALSE, $university_id, $title);
        }
        else
        {
            $config["total_rows"] = $this->Past_Exam_Solution_model->record_count();
        }



        $config["base_url"] = base_url() . "admin123/Past-Exam-Solution/index";
        // $config["total_rows"] = $this->Past_Exam_Solution_model->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $config['use_page_numbers']  = TRUE;
        $config['reuse_query_string']=TRUE;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $offset = 0;
        if($page > 1)
        {
            $offset = ($page * $config["per_page"]) - $config["per_page"];
        }

        $data['sn'] = $offset == 0 ? 1 : $offset + 1;
        if($id || $university_id || $title)
        {
            $data['rows'] = $this->Past_Exam_Solution_model->filter($config["per_page"], $offset, $id, FALSE, $university_id, $title);
        }
        else
        {
            $data['rows'] = $this->Past_Exam_Solution_model->getRows($config["per_page"], $offset);
        }
        // $data["rows"] = $this->Past_Exam_Solution_model->getRows($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);
        $this->load->view($this->config->item('template_dir_admin') . 'menu');
        $this->load->view($this->config->item('template_dir_admin') . 'past-exam-solution', $data);
    }

    public function view($id = NULL)
    {
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $header['page_title'] = 'Past Exam Solution Details';
        $data['row'] = $this->Past_Exam_Solution_model->getRows(0, 0, $id);

        $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_admin') . 'past-exam-solution-details', $data);  // load content view
    }

    public function create()
    {
        $user_id = (int) $this->session->userdata("user_id");

        // $config['upload_path']          = '/var/www/html/academicianhelp-past-exam-solution-files/';
        // $config['upload_path']          = '/home/brilll8h/public_html/academicianhelp-past-exam-solution-files/';
        $config['upload_path']          = $this->upload_save_path;
        $config['allowed_types']        = 'csv|txt|zip|rar|gz|doc|docx|xls|xlsx|pdf|pub|epub|ppt|pptx|iso|jpg|jpeg|png|gif';
        $config['max_size']             = 102400; // 100MB
        $new_name = "";
        $user_id = $this->session->userdata("user_id");

        if(isset($_FILES["file_url"]['name']))
        {
            $file_name = $_FILES["file_url"]['name'];
            $tmp = explode(".", $file_name);
            $file_ext = end($tmp);

            array_pop($tmp);
            $new_name = implode('', $tmp);
            $new_name = str_replace(' ', '', $new_name);

            $config['file_name'] = $new_name . '_' . date('YmdHis') . "_$user_id." . $file_ext;
            $_POST['file_url'] = $config['file_name'];
        }

        $this->load->library('upload', $config);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('university_id', 'University', 'trim|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|max_length[8000]');
        $this->form_validation->set_rules('price', 'Price', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|max_length[800000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);
        }
        elseif((is_null($this->input->post('university_id'))) || ($this->input->post('university_id') == 0))
        {
            $errors = "Please select university!";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', 1);
        }
        elseif( !$this->upload->do_upload('file_url'))
        {
            $errors = str_replace("<p>","", $this->upload->display_errors());
            $errors = str_replace("</p>","", $errors);
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', 1);
        }
        else
        {
            $this->Past_Exam_Solution_model->add();
            $this->session->set_flashdata('error', 'Record(s) added successfully.');
            $this->session->set_flashdata('error_code', 0);
        }

        redirect("/admin123/Past-Exam-Solution");
    }

    public function edit($id)
    {
        $id = (int) $id;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('university_id', 'University', 'trim|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|max_length[8000]');
        $this->form_validation->set_rules('price', 'Price', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|max_length[800000]');
        $this->form_validation->set_rules('file_url_old', '--file--', 'trim');

        $data['universities'] = $this->University_model->getRowsDropDown();

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $data['error'] = $this->session->flashdata('error');
            $data['error_code'] = $this->session->flashdata('error_code');
            $header['page_title'] = 'Update Past Exam Solution';  // set page title
            $data['row'] = $this->Past_Exam_Solution_model->getRows(0, 0, $id);
            if(empty($data['row']))
                redirect("/admin123/Past-Exam-Solution");
            
            $this->load->view($this->config->item('template_dir_admin') . 'header', $header);  // load header view
            $this->load->view($this->config->item('template_dir_admin') . 'menu');  // load menu view
            $this->load->view($this->config->item('template_dir_admin') . 'past-exam-solution-edit', $data);  // load content view
            
        }
        else
        {

            // $config['upload_path']          = '/var/www/html/academicianhelp-past-exam-solution-files/';
            // $config['upload_path']          = '/home/brilll8h/public_html/academicianhelp-past-exam-solution-files/';
            $config['upload_path']          = $this->upload_save_path;
            $config['allowed_types']        = 'csv|txt|zip|rar|gz|doc|docx|xls|xlsx|pdf|pub|epub|ppt|pptx|iso|jpg|jpeg|png|gif';
            $config['max_size']             = 102400; // 100MB
            $new_name = "";
            $user_id = $this->session->userdata("user_id");


            if(isset($_FILES["file_url"]['name']) && !empty($_FILES["file_url"]['name']))
            {
                $file_name = $_FILES["file_url"]['name'];
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);
                $new_name = implode('', $tmp);
                $new_name = str_replace(' ', '', $new_name);

                $config['file_name'] = $new_name . '_' . date('YmdHis') . "_$user_id." . $file_ext;
                $_POST['file_url'] = $config['file_name'];

                $this->load->library('upload', $config);

                if( !$this->upload->do_upload('file_url')) // upload file_url and check if error
                {
                    $errors = str_replace("<p>","", $this->upload->display_errors());
                    $errors = str_replace("</p>","", $errors);
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', 1);
                    redirect("/admin123/Past-Exam-Solution");
                }

                $this->Past_Exam_Solution_model->update($id);

                // delete former file_url
                unlink($config['upload_path'] . $this->input->post('file_url_old'));
            }
            else
            {
                // file not uploaded, update without file_url
                $this->Past_Exam_Solution_model->updateWithoutFile($id);
            }

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Record updated successfully!");

            redirect("/admin123/Past-Exam-Solution");
        }
    }

    public function delete($id)
    {
        $data['row'] = $this->Past_Exam_Solution_model->delete($id);

        $this->session->set_flashdata('error_code', 0);
        $this->session->set_flashdata('error', "Record deleted successfully!");
        redirect('/admin123/Past-Exam-Solution', 'refresh');
    }

    public function download($id)
    {
        $id = (int) $id;

        $this->load->helper('download');
        
        $data['row'] = $this->Past_Exam_Solution_model->getRows(0, 0, $id);

        // $path = '/var/www/html/academicianhelp-past-exam-solution-files/';
        // $path = '/home/brilll8h/public_html/academicianhelp-past-exam-solution-files/';
        $path = $this->upload_save_path;

        if(!empty($data['row']))
        {
            force_download($path . $data['row']['file_url'], NULL, TRUE);
        }
        else
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Unable to download file!");
            redirect('/admin123/Past-Exam-Solution', 'refresh');
        }
    }
}
