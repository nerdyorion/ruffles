<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Category_model');
        $this->load->model('Faq_model');
    }

    public function index()
    {
        $header['page_title'] = 'FAQs';
        $header['page_desc'] = 'FAQs | Buy Chocolate Cakes and other variants.';

        $header['categories_all'] = $this->Category_model->getRowsDropDown();

        $data["rows"] = $this->Faq_model->getRows(0, 0);

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('faqs', $data);  // load content view
    }
}