<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Artist_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->model('User_model');
            $this->load->model('Country_model');
            $this->load->model('Category_Agent_model');
            $this->load->model('Category_Talent_model');
            $this->load->model('Category_Social_Influencer_model');
            //$this->load->helper('url_helper');
    }

    public function index()
    {
        $header['page_title'] = 'My Profile';
        $data['row'] = $this->User_model->getRows($this->session->userdata('user_id'));
        if(empty($data['row']))
            redirect('/artist/logout');

        // echo "<pre>"; var_dump($data['row']); die;
        $data['role_id'] = (int) $this->session->userdata('user_role_id');
        $data['countries'] = $this->Country_model->getRows();
        $data['categories'] = $this->Category_Talent_model->getRows();
        $data['categories_social_influencer'] = $this->Category_Social_Influencer_model->getRows();

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $this->load->view($this->config->item('template_dir_artist') . 'header', $header);  // load header view
        $this->load->view($this->config->item('template_dir_artist') . 'menu');  // load menu view
        $this->load->view($this->config->item('template_dir_artist') . 'profile', $data);  // load content view
    }

    public function updateBasicInfo()
    {
        $role_id = (int) $this->session->userdata('user_role_id');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('stage_name', 'Stage Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email');
        $this->form_validation->set_rules('category_id', 'Category', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('country_id', 'Country', 'trim|required|max_length[255]');

        $this->form_validation->set_rules('phone', 'Phone', 'numeric|max_length[13]');
        $this->form_validation->set_rules('gender', 'Gender', 'required|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/artist/profile');
        }
        else
        {
            $this->User_model->updateProfileBasicInfo($this->session->userdata('user_id'));  // update profile

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Profile updated successfully!");
            
            redirect('/artist/profile', 'refresh');
        }
    }

    public function updateBankInfo()
    {
        $role_id = (int) $this->session->userdata('user_role_id');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/artist/profile');
        }
        else
        {
            $this->User_model->updateProfileBankInfo($this->session->userdata('user_id'));  // update profile

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Profile updated successfully!");
            
            redirect('/artist/profile', 'refresh');
        }
    }

    public function updateGenre()
    {
        $role_id = (int) $this->session->userdata('user_role_id');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('genre_jazz', 'Jazz', 'trim|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/artist/profile');
        }
        else
        {
            $this->User_model->updateGenre($this->session->userdata('user_id'));  // update profile

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Profile updated successfully!");
            
            redirect('/artist/profile', 'refresh');
        }
    }

    public function addService()
    {
        $role_id = (int) $this->session->userdata('user_role_id');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Service Name', 'trim|max_length[20000]');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/artist/profile');
        }
        else
        {
            $this->User_model->updateProfileAddService($this->session->userdata('user_id'));  // update profile

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Profile updated successfully!");
            
            redirect('/artist/profile', 'refresh');
        }
    }

    public function updateProfilePic()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('image_url_old', '--img--', 'trim');

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect('/artist/profile');
        }
        else
        {
            // for image upload
            $config['upload_path']          = './assets/artist/images/users/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10240; // 10MB
            $new_name = "";

            $user_id = $this->session->userdata("user_id");
            $full_name = str_replace(" ","_", strtolower($this->session->userdata("user_full_name")));
            
            if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
            {
                $file_name = strtolower(trim($_FILES["image"]['name']));
                $tmp = explode(".", $file_name);
                $file_ext = end($tmp);

                array_pop($tmp);
                $new_name = $full_name . '_' . implode('', $tmp);

                $config['file_name'] = strtolower($new_name . '_' . date("YmdHis"));
                $config['file_name_thumb'] = 'thumb_' . $config['file_name'];

                $this->load->library('upload', $config);

                if( !$this->upload->do_upload('image')) // upload image and check if error
                {
                    $errors = str_replace("<p>","", $this->upload->display_errors());
                    $errors = str_replace("</p>","", $errors);
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', 1);
                    redirect("/artist/profile");
                }
                else // upload successful .. now resize
                {
                    $image_data =   $this->upload->data();

                    $configer =  array(
                      'image_library'   => 'gd2',
                      'source_image'    =>  strtolower($image_data['full_path']),
                      'maintain_ratio'  =>  TRUE,
                      'width'           =>  650,
                      'height'          =>  650,
                      );
                    $this->load->library('image_lib', $configer);
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                }
                // for generate thumb image
                $config_resize['image_library'] = 'gd2'; // gd2
                $config_resize['source_image'] = $config['upload_path'] . $config['file_name'] . '.' . $file_ext;

                $config_resize['create_thumb'] = TRUE;
                $config_resize['maintain_ratio'] = TRUE;
                $config_resize['width']         = 40;
                $config_resize['height']       = 40;

                $this->load->library('image_lib', $config_resize);
                $this->image_lib->initialize($config_resize);


                if( !$this->image_lib->resize())
                {
                    $errors = str_replace("<p>","", $this->image_lib->display_errors());
                    $errors = str_replace("</p>","", $errors);
                    $this->session->set_flashdata('error', $errors);
                    $this->session->set_flashdata('error_code', 1);
                    redirect("/artist/profile");
                }

                $_POST['image_url'] =  'assets/artist/images/users/' . $config['file_name'] . '.' . $file_ext;

                $this->User_model->updateProfilePic($this->session->userdata('user_id'));  // update profile

                $this->session->set_flashdata('error_code', 0);
                $this->session->set_flashdata('error', "Profile updated successfully!");
            }
            else
            {
                // do nothing
            }
            
            redirect('/artist/profile', 'refresh');
        }
    }
}
