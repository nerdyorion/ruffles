<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends Artist_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            $this->load->helper("url");
            $this->load->model('Analytics_model');
            $this->load->library("pagination");
    }

    public function index()
    {
        $user_id = (int) $this->session->userdata("user_id");
        $username = $this->session->userdata("user_username");

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        $header['page_title'] = 'RBT Analytics';
        $data['username'] = $username;

        // $data["row"] = $this->Analytics_model->getRowByTalentID($config["per_page"], $offset, $user_id);
        $data["row"] = $this->Analytics_model->getRowByTalentID($user_id);

        $this->load->view($this->config->item('template_dir_artist') . 'header', $header);
        $this->load->view($this->config->item('template_dir_artist') . 'menu');
        $this->load->view($this->config->item('template_dir_artist') . 'analytics', $data);
    }
}
