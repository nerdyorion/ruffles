<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            /*
            if(!is_logged_in())  // check if logged in
            {
                //redirect to login
                redirect('/login');
            }
            */
            //$this->load->helper('url_helper');
            $this->load->model('User_model');
            $this->load->model('Country_model');

            // NB: Roles
            // Role ID 1 - Super Administrator
            // Role ID 2 - Administrator
            // Role ID 3 - Customer
    }

    public function index()
    {
        $header['page_title'] = 'Register';
        $header['page_desc'] = $header['page_title'] .' | Buy Chocolate Cakes and other variants.';
        $header['categories_all'] = $this->Category_model->getRowsDropDown();

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        $data['countries'] = $this->Country_model->getRowsDropDown();

        $data['rows'] = '';
        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('register', $data);  // load content view
    }

    public function secure()
    {
        $role_id = 3;

        $_POST['role_id'] = $role_id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[255]|valid_email');
        $this->form_validation->set_rules('country_id', 'Country', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[8000]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|max_length[8000]|matches[password]');

        $full_name = ucfirst(trim($this->input->post('first_name'))) . ' ' . ucfirst(trim($this->input->post('last_name'))) ;
        $email = trim($this->input->post('email'));
        $register_route = '/register';

        // You could set the title in session like above for example
        $this->session->set_flashdata('first_name', $this->input->post('first_name'));
        $this->session->set_flashdata('last_name', $this->input->post('last_name'));
        $this->session->set_flashdata('country_id', $this->input->post('country_id'));
        $this->session->set_flashdata('email', $this->input->post('email'));

        if($this->User_model->emailExists($email))
        {
            $errors = "You are already registered. <a href='login'>Please login to proceed &raquo;</a>";
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect($register_route);
        }
        elseif ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            $this->session->mark_as_flash('validation_errors'); 

            redirect($register_route);
        }
        else
        {
            $insert_id = $this->User_model->addSignup();

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Registration successful. <a href='login'>Please login to proceed &raquo;</a>");

            $first_name = trim($this->input->post('first_name'));
            $last_name = trim($this->input->post('last_name'));
            $email = trim($this->input->post('email'));

            $to = $email;
            $to_name = "$first_name $last_name";

            // add notification to admin dashboard
            $notification_user_id = 0; // for super admin and admin

            if ($role_id == 3) // customer
            {
                $notification_message = 'New customer sign-up: <a href="' . base_url() . 'admin123/users/view/' . $insert_id . '">' . $first_name . ' ' . $last_name . '</a>';
            }
            add_notification($notification_user_id, $notification_message);
            // --------------------------------------------------

            redirect($register_route);
        }
    }

    public function userExists($username)
    {
        echo $this->User_model->usernameExists($username);
    }
}
