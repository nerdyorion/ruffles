<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Cart extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $header['page_title'] = 'Cart';
        $header['page_desc'] = $header['page_title'] .' | Buy Chocolate Cakes and other variants.';
        $header['categories_all'] = $this->Category_model->getRowsDropDown();

        
        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');

        // ------------- from fetch
        $cart_items = array();
        $cart_total = 0;
        
        $output = array();

        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))) // cart not empty
        {
            foreach ($this->session->userdata('cart') as $key => $item) {
                // var_dump($this->session->userdata('cart')); die;
                $cart = array(
                    'product_id' => $key,
                    'product_name' => $item['product_name'],
                    'product_name_short' => urldecode(ellipsize($item['product_name'], 33)),
                    'product_price' => number_format($item['product_price']),
                    'product_image' => urldecode($item['product_image']),
                    'product_quantity' => number_format($item['product_quantity'])
                );

                $cart_total += ($item['product_price'] * $item['product_quantity']);

                $cart_items[] = array($cart);
            }
            // add format
            $cart_total = number_format($cart_total); 
        }
        else // cart empty
        {
            // do nothing
        }

        $output = array(
            'cart_items' => $cart_items,
            'cart_total' => $cart_total,
        );
        // ------------- from getch
        $data['rows'] = $output;

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('cart', $data);  // load content view
    }

    public function add($product_id, $product_price, $product_name, $product_image)
    {
        // $this->session->unset_userdata('cart');
        // echo "product_id: $product_id, product_price: $product_price, product_name: $product_name"; die;
        $product_id = (int) $product_id;
        $product_price = (float) $product_price;
        $product_name = filter_var($product_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $product_image = filter_var($product_image, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        // add to existing cart session
        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart')))
        {
            // check if item already in cart
            if(isset($this->session->userdata('cart')[$product_id]))
            {
                // do nothing, here's not a quantity based something
            }
            else
            {
                // add new item to existing cart
                $cart = array(
                    'product_id' => $product_id,
                    'product_price' => $product_price,
                    'product_name' => $product_name,
                    'product_image' => $product_image,
                    'product_quantity' => 1,
                );

                $former_items = $this->session->userdata('cart');
                $former_items[(int) $product_id] = $cart;
                // $added_item_to_former_items = array_merge($former_items, $cart); // add new item

                // var_dump($former_items); die;

                $this->session->set_userdata('cart', $former_items);
            }
        }
        else // add to new cart [we use product_id as keys to the cart array]
        {
            $cart = array(
                (int) $product_id => array(
                    'product_id' => $product_id,
                    'product_price' => $product_price,
                    'product_name' => $product_name,
                    'product_image' => $product_image,
                    'product_quantity' => 1
                )
            );

            $this->session->set_userdata('cart', $cart);
        }
        echo 1;
        // var_dump($this->session->userdata('cart')); die;
    }

    public function update($product_id, $product_quantity, $ajax = TRUE)
    {
        $product_id = (int) $product_id;
        $product_quantity = (int) $product_quantity;

        // add to existing cart session
        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart')))
        {
            // check if item already in cart
            if(isset($this->session->userdata('cart')[$product_id]))
            {
                // update quantity
                $cart = $this->session->userdata('cart');
                // $cart_item = $cart[$product_id];
                // $cart_item['product_quantity'] = $product_quantity;
                $cart[$product_id]['product_quantity'] = $product_quantity;

                // set session cart to updated cart
                $this->session->set_userdata('cart', $cart);
            }
            else
            {
                // do nothing
            }
        }

        $this->session->set_flashdata('error', 'Cart updated successfully.');
        $this->session->set_flashdata('error_code', 0);

        if($ajax === TRUE)
        {
            echo 1;
        }
        else
        {
            redirect('cart');
        }
    }

    public function fetch()
    {
        // echo "about to fetch :) ..."; die;
        $cart_items = array();
        $cart_total = 0;
        
        $output = array();

        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))) // cart not empty
        {
            foreach ($this->session->userdata('cart') as $key => $item) {
                // var_dump($this->session->userdata('cart')); die;
                $cart = array(
                    'product_id' => $key,
                    'product_name' => $item['product_name'],
                    'product_name_short' => urldecode(ellipsize($item['product_name'], 33)),
                    'product_price' => number_format($item['product_price']),
                    'product_image' => urldecode($item['product_image']),
                    'product_quantity' => number_format($item['product_quantity'])
                );

                $cart_total += ($item['product_price'] * $item['product_quantity']);

                $cart_items[] = array($cart);
            }
            // add format
            $cart_total = number_format($cart_total);
        }
        else // cart empty
        {
            // do nothing
        }

        $output = array(
            'cart_items' => $cart_items,
            'cart_total' => $cart_total,
        );

        echo json_encode($output);
    }

    public function remove($product_id)
    {
        $product_id = (int) $product_id;

        // chck if there's something in cart
        if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart')))
        {
            // check if item already in cart
            if(isset($this->session->userdata('cart')[$product_id]))
            {
                $former_items = $this->session->userdata('cart');
                unset($former_items[(int) $product_id]);

                $this->session->set_userdata('cart', $former_items);
            }
            else
            {
                // do nothing
            }
        }
        else // no cart
        {
                // do nothing
        }
        echo 1;
    }

    private function SEND_MAIL($inquirer_email, $inquirer_name, $subject, $message, $attachments) {

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $this->email->to('admin@academicianhelp.co.uk', 'AcademicianHelp');
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->reply_to($inquirer_email, $inquirer_name);
        if(!empty($attachments))
        {
            foreach ($attachments as $attachment) {
                if(file_exists($attachment))
                {
                    $this->email->attach($attachment);         // Add attachments
                }
            }
        }
        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send())
        {
            return TRUE;
        }
        else
        {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $this->email->print_debugger() . "<br />";
            // error_log($this->email->print_debugger());
            error_log($error);
            return FALSE;
        }
    }

    private function ztest()
    {

        $email = 'nerdyorion@gmail.com';
        $subject = 'Test Subject CI';
        $message = 'Test Message';


        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://md-59.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'smtp-mailer@brilloconnetz.com', 
            'smtp_pass' => 'smtp-mailer123', 
            'mailtype' => 'html',
            'charset' => 'utf-8', // utf-8 iso-8859-1
            'wordwrap' => TRUE
        );


        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $this->email->to($email);
        $this->email->bcc($this->config->item('admin_bcc_email'));
        $this->email->reply_to('admin@academicianhelp.co.uk', 'AcademicianHelp Admin');
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->attach('/var/www/html/academicianhelp/assets/images/logo.png');
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            error_log($this->email->print_debugger());
        }
    }

    private function zSEND_MAIL($inquirer_email, $inquirer_name, $subject, $message, $attachments) {
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'md-59.webhostbox.net';  // Specify main and backup SMTP servers md-59.webhostbox.net 465 // mail.brilloconnetz.com 25
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'smtp-mailer@brilloconnetz.com';                 // SMTP username
        $mail->Password = 'smtp-mailer123';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to
        $mail->Timeout =   15;                                // set the timeout (seconds)

        $mail->setFrom('website@academicianhelp.co.uk', 'AcademicianHelp Website');
        $mail->addAddress('admin@academicianhelp.co.uk', 'AcademicianHelp');     // Add a recipient 
        $mail->addReplyTo($inquirer_email, $inquirer_name);
        // $mail->addAddress('admin@academicianhelp.co.uk', 'AcademicianHelp');     // Add a recipient
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('em@jj.com', 'EM Support');
        //$mail->addBCC('bcc@example.com');
        if(!empty($attachments))
        {
            foreach ($attachments as $attachment) {
                if(file_exists($attachment))
                {
                    $mail->addAttachment($attachment);         // Add attachments
                }
            }
        }

        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = strip_tags($message);

        if(!$mail->send()) {
            $error = "Message could not be sent.<br />";
            $error .= "Message: $message" . "<br />";
            $error .= "Mailer Error: " . $mail->ErrorInfo . "<br />";
            error_log($error);
            return FALSE;
        } else {
            return TRUE;
        }
    }
}