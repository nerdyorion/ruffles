<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!is_logged_in())  // check if logged in
            {
                redirect('/artists');
            }
            if(!is_agent())  // check if agent
            {
                redirect('/artists');
            }
            $this->load->model('Booking_model');
            $this->load->model('User_model');
    }

    public function index($username)
    {

        $data['error'] = $this->session->flashdata('error');
        $data['error_code'] = $this->session->flashdata('error_code');
        
        if (substr($username, 0, 1) == '@'){
            $username = substr($username, 1);
        }

        $data['row'] = $this->User_model->getRowByUsername($username);
        $data['event_time'] = $this->event_time();
        $data['content_industry'] = $this->content_industry();
        $data['content_budget'] = $this->content_budget();
        $data['content_tier'] = $this->content_tier();
        $data['content_campaign_duration'] = $this->content_campaign_duration();

        if(empty($data['row']))
        {
            redirect("/artists");
        }

        $header['page_title'] = 'Book ' . $data['row']['stage_name'];

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('book', $data);  // load content view
    }

    public function secure($username)
    {
        if (substr($username, 0, 1) == '@'){
            $username = substr($username, 1);
        }

        $data['row'] = $this->User_model->getRowByUsername($username);

        if(empty($data['row']))
        {
            redirect("/artists");
        }

        $talent_id = $data['row']['id'];

        $this->load->library('form_validation');
        $this->form_validation->set_rules('event_name', 'Event Name', 'trim|required|max_length[2000]');
        $this->form_validation->set_rules('event_date', 'Event Date', 'trim|required');
        $this->form_validation->set_rules('event_start_time', 'Event Start Time', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('event_end_time', 'Event End Time', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('event_location', 'Event Location', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('event_description', 'Event Description', 'trim|required');

        $full_name = $this->session->userdata('user_full_name');
        $agent_email = strtolower($this->session->userdata("user_email"));
        $agent_id = (int) $this->session->userdata("user_id");

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect("/book/@" . $username);
        }
        elseif ($this->User_model->isSuspended($agent_id))
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Your account has been suspended. Kindly contact us to resolve.");

            redirect("/book/@" . $username);
        }
        elseif ($this->Booking_model->isNotAvailable($talent_id, trim($this->input->post('event_date'))) === true)
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Booking not available for this date (" . trim($this->input->post('event_date')) . "). Kindly choose a different date or contact us.");

            redirect("/book/@" . $username);
        }
        else
        {
            $_POST['talent_id'] = $talent_id;

            $this->Booking_model->add();

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Booking request successful. <a href='agent/bookings'>Please proceed to dashboard for more info &raquo;</a>");


            $artist_name = $data['row']['stage_name'];
            $to = $agent_email;
            $to_name = $full_name;
            $subject = "Booking Request";

            $event_details = "<b>Event Name</b>: " . trim($this->input->post('event_name')) . " <br />";
            $event_details .= "<b>Date</b>: " . trim($this->input->post('event_date')) . " <br />";
            $event_details .= "<b>Start Time</b>: " . trim($this->input->post('event_start_time')) . " <br />";
            $event_details .= "<b>End Time</b>: " . trim($this->input->post('event_end_time')) . " <br />";
            $event_details .= "<b>Location</b>: " . trim($this->input->post('event_location')) . " <br />";
            $event_details .= "<b>Description</b>: " . trim($this->input->post('event_description')) . " <br />";


            $message = $this->template_booking_request($to_name, $artist_name, $event_details);

            // send booking request notification mail to agent
            sendmail($to, $to_name, $subject, $message);
            // ---------------------------------------

            // send booking request notification mail to artist
            $to_name = $artist_name;
            $agent_name = $full_name;
            $message = $this->template_to_artist_booking_request($to_name, $agent_name, $event_details);

            sendmail($to, $to_name, $subject, $message);
            // --------------------------------------------


            // add notification to admin dashboard
            $notification_user_id = 0; // for super admin and admin

            $notification_message = 'Agent ' . $agent_name . ' booked for <a href="' . base_url() . '@' . $username . '">' . $to_name . '</a>';
            add_notification($notification_user_id, $notification_message);
            // -----------------------------------------------------------------
            
            // add notification to artist dashboard
            $notification_user_id = $talent_id; // for booked artist

            $notification_message = 'New booking request: ' . $agent_name;
            add_notification($notification_user_id, $notification_message);
            // ----------------------------------------------------------------

            redirect("/book/@" . $username);
        }

    }

    public function secureCC($username) // content creator booking
    {
        if (substr($username, 0, 1) == '@'){
            $username = substr($username, 1);
        }

        $data['row'] = $this->User_model->getRowByUsername($username);

        $data['content_industry'] = $this->content_industry();
        $data['content_budget'] = $this->content_budget();
        $data['content_tier'] = $this->content_tier();
        $data['content_campaign_duration'] = $this->content_campaign_duration();

        if(empty($data['row']))
        {
            redirect("/artists");
        }

        $talent_id = $data['row']['id'];

        $this->load->library('form_validation');
        $this->form_validation->set_rules('content_brief', 'Content Brief', 'trim|required');
        $this->form_validation->set_rules('content_industry', 'Content Industry', 'trim|required');

        $full_name = $this->session->userdata('user_full_name');
        $agent_email = strtolower($this->session->userdata("user_email"));
        $agent_id = (int) $this->session->userdata("user_id");

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect("/book/@" . $username);
        }
        elseif ($this->User_model->isSuspended($agent_id))
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Your account has been suspended. Kindly contact us to resolve.");

            redirect("/book/@" . $username);
        }
        /*
        elseif ($this->Booking_model->isNotAvailable($talent_id, trim($this->input->post('event_date'))) === true)
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Booking not available for this date (" . trim($this->input->post('event_date')) . "). Kindly choose a different date or contact us.");

            redirect("/book/@" . $username);
        }
        */
        else
        {
            $_POST['talent_id'] = $talent_id;

            $this->Booking_model->addCC(); // content creator

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Booking request successful. <a href='agent/bookings'>Please proceed to dashboard for more info &raquo;</a>");


            $artist_name = $data['row']['stage_name'];
            $to = $agent_email;
            $to_name = $full_name;
            $subject = "Booking Request (Create Content)";

            $content_industry_id = getArrayKey($data['content_industry'], 'id', trim($this->input->post('content_industry')));
            $content_industry = isset($data['content_industry'][$content_industry_id]) ? $data['content_industry'][$content_industry_id] : '-';

            // var_dump($content_industry); die;

            $event_details = "<b>Brief</b>: " . trim($this->input->post('content_brief')) . " <br />";
            $event_details .= "<b>Industry</b>: " . $content_industry['value'] . " <br />";


            $message = $this->template_booking_request($to_name, $artist_name, $event_details);

            // var_dump($message); die;

            // send booking request notification mail to agent
            sendmail($to, $to_name, $subject, $message);
            // ---------------------------------------

            // send booking request notification mail to artist
            $to_name = $artist_name;
            $agent_name = $full_name;
            $message = $this->template_to_artist_booking_request($to_name, $agent_name, $event_details);


            // var_dump($message); die;

            sendmail($to, $to_name, $subject, $message);
            // --------------------------------------------


            // add notification to admin dashboard
            $notification_user_id = 0; // for super admin and admin

            $notification_message = 'Agent ' . $agent_name . ' booked for <a href="' . base_url() . '@' . $username . '">' . $to_name . '</a>';
            add_notification($notification_user_id, $notification_message);
            // -----------------------------------------------------------------
            
            // add notification to artist dashboard
            $notification_user_id = $talent_id; // for booked artist

            $notification_message = 'New booking request: ' . $agent_name;
            add_notification($notification_user_id, $notification_message);
            // ----------------------------------------------------------------

            redirect("/book/@" . $username);
        }

    }

    public function secureA($username) // content amplifier booking
    {
        if (substr($username, 0, 1) == '@'){
            $username = substr($username, 1);
        }

        $data['row'] = $this->User_model->getRowByUsername($username);

        $data['content_industry'] = $this->content_industry();
        $data['content_budget'] = $this->content_budget();
        $data['content_tier'] = $this->content_tier();
        $data['content_campaign_duration'] = $this->content_campaign_duration();

        if(empty($data['row']))
        {
            redirect("/artists");
        }

        $talent_id = $data['row']['id'];

        $this->load->library('form_validation');
        $this->form_validation->set_rules('content_industry', 'Content Industry', 'trim|required');
        $this->form_validation->set_rules('content_budget', 'Content Budget', 'trim|required');
        $this->form_validation->set_rules('content_tier', 'Content Tier', 'trim|required');
        $this->form_validation->set_rules('content_campaign_duration', 'Content Campaign Duration', 'trim|required');

        $full_name = $this->session->userdata('user_full_name');
        $agent_email = strtolower($this->session->userdata("user_email"));
        $agent_id = (int) $this->session->userdata("user_id");

        if ($this->form_validation->run() === FALSE)
        {
            $errors = str_replace("<p>","", validation_errors());
            $errors = str_replace("</p>","", trim($errors));
            $this->session->set_flashdata('error', $errors);
            $this->session->set_flashdata('error_code', empty($errors) ? 0 : 1);

            redirect("/book/@" . $username);
        }
        elseif ($this->User_model->isSuspended($agent_id))
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Your account has been suspended. Kindly contact us to resolve.");

            redirect("/book/@" . $username);
        }
        /*
        elseif ($this->Booking_model->isNotAvailable($talent_id, trim($this->input->post('event_date'))) === true)
        {
            $this->session->set_flashdata('error_code', 1);
            $this->session->set_flashdata('error', "Booking not available for this date (" . trim($this->input->post('event_date')) . "). Kindly choose a different date or contact us.");

            redirect("/book/@" . $username);
        }
        */
        else
        {
            $_POST['talent_id'] = $talent_id;

            $this->Booking_model->addA(); // amplifier

            $this->session->set_flashdata('error_code', 0);
            $this->session->set_flashdata('error', "Booking request successful. <a href='agent/bookings'>Please proceed to dashboard for more info &raquo;</a>");


            $artist_name = $data['row']['stage_name'];
            $to = $agent_email;
            $to_name = $full_name;
            $subject = "Booking Request (Create Content)";

            $content_industry_id = getArrayKey($data['content_industry'], 'id', trim($this->input->post('content_industry')));
            $content_industry = isset($data['content_industry'][$content_industry_id]) ? $data['content_industry'][$content_industry_id] : '-';

            $content_budget_id = getArrayKey($data['content_budget'], 'id', trim($this->input->post('content_budget')));
            $content_budget = isset($data['content_budget'][$content_budget_id]) ? $data['content_budget'][$content_budget_id] : '-';

            $content_tier_id = getArrayKey($data['content_tier'], 'id', trim($this->input->post('content_tier')));
            $content_tier = isset($data['content_tier'][$content_tier_id]) ? $data['content_tier'][$content_tier_id] : '-';

            $content_campaign_duration_id = getArrayKey($data['content_campaign_duration'], 'id', trim($this->input->post('content_campaign_duration')));
            $content_campaign_duration = isset($data['content_campaign_duration'][$content_campaign_duration_id]) ? $data['content_campaign_duration'][$content_campaign_duration_id] : '-';

            // var_dump($content_industry); die;

            $event_details = "<b>Industry</b>: " . $content_industry['value'] . " <br />";
            $event_details .= "<b>Budget</b>: " . $content_budget['value'] . " <br />";
            $event_details .= "<b>Tier</b>: " . $content_tier['value'] . " <br />";
            $event_details .= "<b>Campaign Duration</b>: " . $content_campaign_duration['value'] . " <br />";


            $message = $this->template_booking_request($to_name, $artist_name, $event_details);

            // var_dump($message); die;

            // send booking request notification mail to agent
            sendmail($to, $to_name, $subject, $message);
            // ---------------------------------------

            // send booking request notification mail to artist
            $to_name = $artist_name;
            $agent_name = $full_name;
            $message = $this->template_to_artist_booking_request($to_name, $agent_name, $event_details);

            
            // var_dump($message); die;

            sendmail($to, $to_name, $subject, $message);
            // --------------------------------------------


            // add notification to admin dashboard
            $notification_user_id = 0; // for super admin and admin

            $notification_message = 'Agent ' . $agent_name . ' booked for <a href="' . base_url() . '@' . $username . '">' . $to_name . '</a>';
            add_notification($notification_user_id, $notification_message);
            // -----------------------------------------------------------------
            
            // add notification to artist dashboard
            $notification_user_id = $talent_id; // for booked artist

            $notification_message = 'New booking request: ' . $agent_name;
            add_notification($notification_user_id, $notification_message);
            // ----------------------------------------------------------------

            redirect("/book/@" . $username);
        }

    }

    public function mail($id)
    {
        $artist_name = 'EmperorG';
        $to = 'nerdyorion@gmail.com';
        $to_name = "John Doe";
        $subject = "Booking Request";

        $event_details = "<b>Event Name</b>: Miami <br />";
        $event_details .= "<b>Date</b>: 2017-11-25 <br />";
        $event_details .= "<b>Start Time</b>: 18:00 <br />";
        $event_details .= "<b>End Time</b>: 20:00 <br />";
        $event_details .= "<b>Location</b>: Miami <br />";
        $event_details .= "<b>Description</b>: I'm just checking how lovely the site is. Please ignore. <br />";

        echo $message = $this->template_booking_request($to_name, $artist_name, $event_details);

        // sendmail($to, $to_name, $subject, $message); 
    }

    private function event_time()
    {
        $start = 0;
        $end = 24;
        $period = '';
        $time = '';
        $output = array();

        for($start = $start; $start < $end; $start++ )
        {
            if($start < 12)
            {
                $time = $start;
                $period = 'AM';
            }
            else
            {
                $time = abs($start - 12);
                $period = 'PM';
            }

            if($time == 0) // change 0AM to 12AM
            {
                // echo "12 $period<br />";
                $output[] =  "12 $period";
            }
            else
            {
                // echo "$time $period<br />";
                $output[] =  "$time $period";
            }
        }
        return $output;
    }

    private function content_industry()
    {
        $output = array(
            array('id' => 1, 'value' => 'Investment'),
            array('id' => 2, 'value' => 'Politics'),
            array('id' => 3, 'value' => 'Product'),
            array('id' => 4, 'value' => 'Retail'),
            array('id' => 5, 'value' => 'Sales'),
            array('id' => 6, 'value' => 'Technology')
        );
        return $output;
    }

    private function content_budget()
    {
        $output = array(
            array('id' => 1, 'value' => '100K TO 500K'),
            array('id' => 2, 'value' => '500K TO 1M'),
            array('id' => 3, 'value' => '1M TO 3M'),
            array('id' => 4, 'value' => '3M TO 5M'),
            array('id' => 5, 'value' => '5M AND ABOVE')
        );
        return $output;
    }

    private function content_tier()
    {
        $output = array(
            array('id' => 1, 'value' => '1'),
            array('id' => 2, 'value' => '2'),
            array('id' => 3, 'value' => '3')
        );
        return $output;
    }

    private function content_campaign_duration()
    {
        $output = array(
            array('id' => 1, 'value' => '1 week'),
            array('id' => 2, 'value' => '1 month'),
            array('id' => 3, 'value' => '6 months'),
            array('id' => 4, 'value' => '1 year')
        );
        return $output;
    }

    private function template_booking_request($to_name, $artist_name, $event_details)
    {
        $url = $this->config->base_url() . 'login';

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
            <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
                            Booking Request Submission
                        </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Hello $to_name,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Your booking request for <b>$artist_name</b> has been submitted for approval.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        $event_details
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Login now for more info and payment:
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Login &raquo;</a>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing The Total Package.
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <!--TTP is a registered trademark in Nigeria. 
                    <br />No 4, Whales Street,
                    <br />Admiralty Way, Landon,
                    <br />Lagos, Nigeria
                    <br />Phone: 0123456789-->
                    <br />info@ttp.dev, support@ttp.dev
                    <br />&copy; " . date("Y") . " The Total Package.</td>
                        </tr></table></div></div>
                </td>
                <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
            </tr></table></body>
        </html>";
    }

    private function template_to_artist_booking_request($to_name, $agent_name, $event_details)
    {
        $url = $this->config->base_url() . 'login';

        return "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\">

        <body style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\">

        <table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0; padding: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
        <td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; width: 100% !important; margin: 0 auto; padding: 0;\" valign=\"top\">
            <div class=\"content\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 10px;\">
                <table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; padding: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"alert alert-warning\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #4aa3df; margin: 0; padding: 20px;\" align=\"center\" bgcolor=\"#4aa3df\" valign=\"top\">
                            Booking Request Submission
                        </td>
                    </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">
                            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Hello $to_name,
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        You have a booking request from <b>$agent_name</b>. Event Details below.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        $event_details
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Login now for more info.
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        <a href=\"$url\" class=\"btn-primary\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">Login &raquo;</a>
                                    </td>
                                </tr><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">
                                        Thanks for choosing The Total Package.
                                    </td>
                                </tr></table></td>
                    </tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">
                    <table width=\"100%\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><tr style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; padding: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">
                    <!--TTP is a registered trademark in Nigeria. 
                    <br />No 4, Whales Street,
                    <br />Admiralty Way, Landon,
                    <br />Lagos, Nigeria
                    <br />Phone: 0123456789-->
                    <br />info@ttp.dev, support@ttp.dev
                    <br />&copy; " . date("Y") . " The Total Package.</td>
                        </tr></table></div></div>
                </td>
                <td style=\"font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0;\" valign=\"top\"></td>
            </tr></table></body>
        </html>";
    }
}