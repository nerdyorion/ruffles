<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Web_Development extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $header['page_title'] = 'Web Development';
        $header['page_desc'] = 'This is a one stop website for academic help with dissertation, final year projects, essays, coursework, assignment as well as tutoring for computer science, information technology, software engine…';

        $data['row'] = '';

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('web-development', $data);  // load content view
    }
}