<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('Portfolio_model');
            $this->load->model('User_model');
    }

    public function view($id)
    {
        $data['row'] = $this->Portfolio_model->getRows(0, 0, $id);

        if(empty($data['row']))
        {
            redirect("/");
        }

        $header['page_title'] = $data['row']['stage_name'];

        // $data['mobile_contents'] = $this->Product_model->getRowsFrontEnd(8, 0, 1); // fetching 8 records for homepage
        // $data['music_crbt'] = $this->Product_model->getRowsFrontEnd(8, 0, 2); // fetching 8 records for homepage

        $this->load->view('header', $header);  // load header view
        // $this->load->view('menu');  // load menu view
        $this->load->view('artist-portfolio-single', $data);  // load content view
    }
}
