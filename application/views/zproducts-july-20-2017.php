  <style type="text/css">
    del:after {
      /*border-color: #F05522;*/
      border-color: #000000;
    }
    .price {
      color: #000000;
    }
  </style>
  <div style="width: 90%; margin: auto;">

    <div class="clearfix"></div>

    <div class="container-fluid text-center bg-grey">
      <!-- <h2>MOBILE CONTENTS</h2> -->
      <!-- <h4>What we have created</h4> -->

      <div class="row text-center">

        <ol class="breadcrumb text-left">
          <li><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
          <!-- <li><a href="#">Library</a></li> -->
          <li class="active"><?php echo $page_title; ?></li>
        </ol>
        <div class="col-sm-12" style="border-bottom: 2px solid #feb25a; margin-bottom: 10px; ">
          <h3 class="text-left"><?php echo $page_title; ?></h3>
        </div>

        <?php if(empty($rows)): ?>
          <div class="col-sm-12">
            <p class="text-muted">No services yet, please check back.</p>
          </div>
        <?php else: ?>
          <?php foreach ($rows as $row): ?>
            <?php $confirm = "Click OK to continue."; 
            // $confirm = "You are about to subscribe to " . htmlspecialchars_decode($row['name']) . ", service costs " . $row['price'] . ", click OK to continue.";
            ?>
            <div class="col-sm-3">
              <div class="thumbnail">
                <a href="<?php echo $row['slug']; ?>" class="thumbnail link-thumbnail" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return confirm(\'' . $confirm .'\');"' : ''; ?>>
                  <img src="assets/images/products/<?php echo $row['image_url']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive" title="Subscribe to <?php echo $row['name']; ?>"><!-- 200x170 -->
                </a>
                <div class="caption">
                  <h4><?php echo $row['name']; ?></h4>
                  <p class="price"><del>N</del><?php echo $row['price']; ?></p>
                  <p><a href="<?php echo $row['slug']; ?>" class="btn btn-primary" role="button" <?php echo (!empty($HTTP_TELCO)) && (!empty($HTTP_MSISDN)) ? 'onclick="return confirm(\'' . $confirm .'\');"' : ''; ?>>Subscribe</a></p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <div class="clearfix"></div>
        
        <div class="col-md-3 pull-right pagination">
          <p><?php echo $links; ?></p>
        </div>
        
      </div>

    </div>

    <div class="clearfix"></div>

  </div>


<?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html>