<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->
  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


    <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

    <!-- <h6>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6> -->
<!--=================================
  side-nav-menu -->

  <!-- <h4 class="title"><u>Cart</u></h4> -->
  <h4>Checkout</h4>

  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 xs-mb-30">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Product</th>
          <th>Quantity</th>
          <th class="text-center">Price</th>
          <th class="text-center">Total</th>
          <th> </th>
        </tr>
      </thead>
      <tbody>
        <?php if(empty($rows['cart_items'])): ?>
         <tr>
           <td class="col-sm-8 col-md-6" colspan="5">
             Your cart is empty! <br />
             <a class="btn btn-primary" href="/">Continue Shopping</a>
           </td>
         </tr>
       <?php else: ?>
        <?php foreach($rows['cart_items'] as $key => $item): ?>
          <tr>
            <td class="col-sm-8 col-md-6">
              <div class="media">
                <a class="thumbnail pull-left" href="products/<?php echo $item[0]['product_id']; ?>"> <img class="media-object" src="assets/images/products/<?php echo urldecode($item[0]['product_image']); ?>" style="width: 65px; height: 65px; margin-right: 5px;"> </a>
                <div class="media-body">
                  <h5 class="media-heading"><a href="products/<?php echo $item[0]['product_id']; ?>"><?php echo urldecode($item[0]['product_name']); ?></a></h5>
                  <!-- <span>Status: </span><span class="text-success"><strong>In Stock</strong></span> -->
                </div>
              </div></td>
              <td class="col-sm-1 col-md-1" style="text-align: center">
                <input type="number" min="1" class="form-control" id="cart_qty_<?php echo $item[0]['product_id']; ?>" value="<?php echo $item[0]['product_quantity']; ?>">
              </td>
              <td class="col-sm-1 col-md-1 text-center"><strong>&pound;<?php echo number_format($item[0]['product_price']); ?></strong></td>
              <td class="col-sm-1 col-md-1 text-center"><strong>&pound;<?php echo number_format($item[0]['product_price'] * $item[0]['product_quantity']); ?></strong></td>
              <td class="col-sm-1 col-md-1">
                <button type="button" class="btn btn-danger btn-sm remove-from-cart" title="Remove" data-id="<?php echo $item[0]['product_id']; ?>">
                  <i class="fa fa-remove"></i>
                </button>
                <button type="button" class="btn btn-danger btn-sm update-cart" title="Update" data-id="<?php echo $item[0]['product_id']; ?>"  data-cart-quantity="cart_qty_<?php echo $item[0]['product_id']; ?>">
                  <i class="fa fa-refresh"></i>
                </button>
              </td>
            </tr>
          <?php endforeach; ?>
        <?php endif; ?>
        <?php if(!empty($rows['cart_items'])): ?>
          <tr>
            <td></td><td></td><td></td>
            <td><h3>Total</h3></td>
            <td class="text-right"><h3><strong>&pound;<?php echo $rows['cart_total']; ?></strong></h3></td>
          </tr>
          <?php echo form_open('/checkout/secure', 'class="form-horizontal"'); ?>
          <tr>
              <td></td>
              <td colspan="2"><h6>Payment Type*</h6></td>
              <td class="text-right" colspan="2">
                <select class="form-control" id="payment_type_id" name="payment_type_id">
                  <option value="0" selected="selected">-- Select --</option>
                  <?php foreach ($payment_types as $row): ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </td>
            </tr>
          <tr>
              <td></td>
              <td colspan="2"><h6>Delivery (Country)*</h6></td>
              <td class="text-right" colspan="2">
                <select class="form-control" id="country_id" name="country_id">
                  <option value="0" selected="selected">-- Select --</option>
                  <?php foreach ($countries as $row): ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                  <?php endforeach; ?>
                </select>
              </td>
            </tr>
          <tr>
              <td></td>
              <td colspan="2"><h6>Delivery (Address)*</h6></td>
              <td class="text-right" colspan="2">
                <!-- <input id="address" name="address" class="form-control" type="text" placeholder="" required="required" /> -->
                <textarea class="form-control" name="address" id="address" maxlength="8000" required="required" rows="4"></textarea>
              </td>
            </tr>
            <tr>
              <td></td><td></td><td></td><td></td>
              <td>
                <button type="submit" class="btn btn-default" style="width: 100%">
                  <i class="fa fa-check"></i> Pay
                </button>
              </td>
            </tr>
             </form>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {

    if($('#first_name').val() != '' && $('#last_name').val() != '' && $('#email').val() != '' && $('#password').val() != '' )
    {
      // $('#cover-spin').show();
    }

    $('.hide-loader').click(function () {
      // $('#cover-spin').hide();
    });

    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>