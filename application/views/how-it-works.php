
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>How It Works</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>How It Works</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

<!--=================================
 faq-->

 <section class="faq white-bg page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="section-title text-center">
        <h6>How we are the best!</h6>
        <h2 class="title-effect">How It Works</h2>
        <!-- <p>why the sky's the limit when using AcademicianHelp.</p> -->
      </div>
    </div>
  </div>
  <div class="row"> <div class="col-lg-12 col-md-12">
    <div class="tab border nav-center">
      <ul class="nav nav-tabs">
       <li class="active"><a href="#research-07" data-toggle="tab"> <i class="fa fa-file-word-o"></i> Upload Work</a></li>
       <li><a href="#design-07" data-toggle="tab"> <i class="fa fa-calculator"></i> Await Estimate</a></li>
       <li><a href="#develop-07" data-toggle="tab"> <i class="fa fa-money"></i> Half Payment</a></li>
       <li><a href="#result-07" data-toggle="tab"> <i class="fa fa-file"></i> Request Draft</a></li>
       <li><a href="#result-08" data-toggle="tab"> <i class="fa fa-check-square-o"></i> Work is Ready</a></li>
       <li><a href="#result-09" data-toggle="tab"> <i class="fa fa-refresh"></i> Amendments</a></li>
     </ul>
     <div class="tab-content">
      <div class="tab-pane fade in active" id="research-07">
        <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
          <!-- <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 mb-60"> -->
            <!-- <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8"> -->
              <div class="feature-text left-icon round border">
                <div class="feature-icon">
                  <span class="ti-file"></span>
                </div>
                <div class="feature-info">
                  <!-- <h5 class="text-back">Many Style Available</h5> -->
                  <p>First place an order with the relevant information regarding your assignment, tutoring  or dissertation.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="design-07">
            <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
              <div class="feature-text left-icon round border">
                <div class="feature-icon">
                  <span class="ti-panel"></span>
                </div>
                <div class="feature-info">
                  <p>After you have uploaded your work, our team of professional will evaluate your requirements and then give an estimated cost for the work.</p>
                </div>
              </div>
            </div>
          </div> 
          <div class="tab-pane fade" id="develop-07">
            <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
              <div class="feature-text left-icon round border">
                <div class="feature-icon">
                  <span class="ti-money"></span>
                </div>
                <div class="feature-info">
                  <p>You will need to make at least a deposit of 50% before we start working on your order.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="result-07">
            <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
              <div class="feature-text left-icon round border">
                <div class="feature-icon">
                  <span class="ti-agenda"></span>
                </div>
                <div class="feature-info">
                  <p>Our team will give you an update whenever you ask for it before deadline.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="result-08">
            <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
              <div class="feature-text left-icon round border">
                <div class="feature-icon">
                  <span class="ti-check-box"></span>
                </div>
                <div class="feature-info">
                  <p>Once the work is ready, our team will contact you to make a final deposit of 50% and then send the work to you.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="result-09">
            <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
              <div class="feature-text left-icon round border">
                <div class="feature-icon">
                  <span class="ti-reload"></span>
                </div>
                <div class="feature-info">
                  <p>If you think something is not done properly or does not meet the requirement or you simply want to change some information, we will be glad to do it at no cost.</p>
                </div>
              </div>
            </div>
          </div>
          <div align="center">
            <a class="button border icon" href="request-quote">
              <span>Order Now</span>
              <i class="fa fa-sign-in"></i>
            </a>
          </div>
        </div> 
      </div>
    </div>
  </div> 
  <div class="row">
    <div class="col-lg-12 text-center mt-40">
     <h6> What are you waiting for? Let's <a class="theme-color" href="request-quote">get started</a> </h6>
   </div>     
 </div>
</div> 
</section>

<!--=================================
 careers-->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->


  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 