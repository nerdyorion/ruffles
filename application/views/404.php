

<div class="col-lg-9">

  <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block img-fluid" src="assets/images/banner1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner3.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <a name="addedToCartAlert" id="addedToCartAlert"></a>
  <div class="bs-callout bs-callout-success text-center hidden d-none" style="margin-top:-40px;">
    <h4 id="addedToCartName">-</h4>
    &pound;<span id="addedToCartPrice">-</span> <br />
    added to Cart <br />
    <a class="btn btn-primary" href="checkout">Checkout</a>
  </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                     <div class="error-block text-center clearfix">
                      <div class="error-text">
                        <h2>404</h2>
                        <span>Error </span>
                      </div>
                      <h1 class="theme-color mb-40">Ooopps :(</h1>
                      <p>The Page you were looking for, couldn't be found.</p>
                   </div>   
                    <div class="error-info">
                        <p class="mb-50">The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
                        <a class="button xs-mb-10 " href="./"><i class="fa fa-home"></i> back to home</a>
                        <a class="button border black" href="javascript:void(0);" onclick="history.go(-1); return false;">&larr; back to Prev page</a>
                    </div>
                </div>
            </div>

</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>