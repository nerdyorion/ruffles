<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->
  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


    <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

    <!-- <h6>Welcome <?php // echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6> -->
<!--=================================
  side-nav-menu -->

  <h4 class="title"><u>Orders</u></h4>

  <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 xs-mb-30">




    <?php if(empty($rows)): ?>
     <p>You have not made any orders yet! <br />
       <a class="button small mt-0" href="Past-Exam-Solution">Continue Shopping</a>
     </p>
   <?php else: ?>
    <div class="table-responsive table-hover">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Product(s)</th>
            <th>Total</th>
            <th>Status</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($rows as $row): ?>
           <tr>
            <td><?php echo $sn++; ?></td>
            <td class="description">
              <?php $products = explode('###', $row['items']); ?>
              <ul style="list-style-type: none;">
                <?php foreach($products as $item_array): ?>
                  <?php $item = explode('@@@', $item_array); ?>
                  <!-- 
                  0 - product_id
                  1 - quantity
                  2 - name
                  3 - price
                  4 - image_url
                  -->
                  <li style="border-bottom: 1px solid #f9f9f9;">
                    <div class="media">
                      <a class="thumbnail pull-left" href="products/<?php echo $item[0]; ?>"> <img class="media-object" src="assets/images/products/<?php echo $item[4]; ?>" style="width: 30px; height: 30px; margin-right: 5px;"> </a>
                      <div class="media-body">
                        <h5 class="media-heading"><a href="products/<?php echo $item[0]; ?>"><?php echo $item[2]; ?></a></h5>
                      </div>
                    </div>
                    <b>&pound;<?php echo $item[3]; ?></b> x<?php echo $item[1]; ?>
                  </li>
                  <?php endforeach; ?>
                </ul>

              </td>
              <td class="price">&pound;<?php echo number_format($row['total_price']); ?></td>
              <td class="price"><mark <?php echo $row['status_id'] != 4 ? 'style="background-color: #33AA22; color: #ffffff;"' : 'style="background-color: #FF0800; color: #ffffff;"'; ?>><?php echo strtoupper($row['status']); ?></mark><br />
                <small>Payment Mode: <?php echo $row['payment_type']; ?></small>
              </td>
              <td class="price"><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <?php endif; ?>
  <div class=" pull-right">

    <nav aria-label="...">
      <ul class="pagination">
        <?php echo $links; ?>
      </ul>
    </nav>
  </div>
</div>
</div>
</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>