<style type="text/css">
.sidebar-widgets-wrap .recent-item .recent-title a {
  color: #73a02e;
  font-weight: normal;
}
.sidebar-widgets-wrap .recent-item .recent-title a:hover {
  color: #333;
  /*color: #84ba3f;*/
  /*font-weight: bold;*/
}
</style>
<style type="text/css">
.dropdown.dropdown-lg .dropdown-menu {
  margin-top: -1px;
  padding: 6px 20px;
}
.input-group-btn .btn-group {
  display: flex !important;
}
.btn-group .btn {
  border-radius: 0;
  margin-left: -1px;
}
.btn-group .btn:last-child {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
  margin-left: 0;
  margin-right: 0;
}
.form-group .form-control:last-child {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
  #adv-search {
    width: 500px;
    margin: 0 auto;
  }
  .dropdown.dropdown-lg {
    position: static !important;
  }
  .dropdown.dropdown-lg .dropdown-menu {
    min-width: 500px;
  }
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: auto;
  border: 3px solid #73AD21;
  z-index: 1;
</style>

<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Free Project Proposals</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Solution Library</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="Free-Project-Proposals">Free Project Proposals</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span><?php echo $page_title; ?></span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

<!--=================================
  shop -->

   <a name="addedToCartAlert" id="addedToCartAlert"></a>
  <section class="shop grid page-section-ptb  alerts-and-callouts">
    <div class="container">
      <div class="row">
        <div class="bs-callout bs-callout-success text-center hidden" style="margin-top:-40px;">
          <h4 id="addedToCartName">-</h4>
          &pound;<span id="addedToCartPrice">-</span> <br />
          added to Cart <br />
          <a class="button small mt-0" href="checkout">Checkout</a>
        </div>
        <div class="col-lg-3 col-md-3">
          <div class="sidebar-widgets-wrap">
           <div class="sidebar-widget mb-40">
            <h5 class="mb-20">search</h5>
            <div class="widget-search">
              <?php echo form_open('Free-Project-Proposals', 'class="form-horizontal", method="get", id="searchForm", onsubmit="return validate();"'); ?>
              <i class="fa fa-search" id="searchIcon"></i>
              <input type="search" id="title" name="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" class="form-control placeholder" placeholder="Search....">
            </form>
          </div>
        </div>
        <div class="sidebar-widget mb-40">
          <h5 class="mb-20">Level</h5>
          <div class="widget-link">
            <ul>
              <?php foreach ($levels as $level): ?>
                <li> <a href="Free-Project-Proposal?level_id=<?php echo $level['id']; ?>"> <i class="fa fa-angle-double-right"></i> <?php echo $level['name']; ?> </a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="sidebar-widget mb-40">
          <h5 class="mb-20">Recent items</h5>
          <?php foreach ($recent_items as $recent_item): ?>
            <div class="recent-item clearfix">
              <div class="recent-info">
                <div class="recent-title">
                 <a href="Free-Project-Proposal?id=<?php echo $recent_item['id']; ?>" class="text-success"><?php echo ellipsize($recent_item['title'], 40); ?></a> 
               </div>
               <div class="recent-meta">
                 <ul class="list-style-unstyled">
                  <li class="color"><?php echo dashIfEmpty($recent_item['level_name']); ?></li>
                </ul>    
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <div class="col-lg-9 col-md-9 sm-mt-40">
    <div class="row">
     <div class="product listing">
      <div class="col-lg-12 col-md-12 col-sm-12 mb-40">

        <div class="product-des text-left">
         <div class="product-title">
          <h3><?php echo dashIfEmpty($row['title']); ?></h3>
         </div>
        <div class="product-price">
          <i class="fa fa-university"></i> <?php echo dashIfEmpty($row['level_name']); ?>
        </div>
        <div class="product-info mt-20">
          <p><?php echo dashIfEmpty($row['description']); ?></p>
          <button class="button small mt-0" href="javascript:void(0);">download</button>
          <button class="button small mt-0" onclick="location.href='Request-Quote';">Get Full Plagiarism Free Version</button>
          <button class="button small mt-0" href="javascript:void(0);" onclick="history.go(-1);">&larr; Back</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="row">
  <div class="col-lg-12 text-center mt-40">
    <h6> Can't find your past examination paper? <a class="theme-color" href="Request-Quote">Upload your past paper to get one of our experts solve it for you</a> </h6>
  </div>     
</div>
</div>
</div>
</section>

<!--=================================
  welcome -->

<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate ()
    {
      var title = document.getElementById("title").value;
      if(title == "" || title == null)
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    $(document).ready(function() {
      $('#searchIcon').click(function () {
        $('#searchForm').submit();
      });
    });
  </script>

</body>
</html> 