
<!--=================================
page-title-->

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>terms and conditions</h1>
          <p>We know the secret of your success</p>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Company</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Terms and Conditions</span> </li>
       </ul>
     </div>
  </div>
</div>
</section>

<!--=================================
page-title -->


<!--=================================
 terms-and-conditions -->

<section class="terms-and-conditions page-section-ptb">
  <div class="container">
  <div class="row">
       <div class="col-lg-12 col-md-12">
        <div class="section-title ">
            <h6>Protecting you</h6>
            <h2 class="title-effect">Terms and Conditions</h2>
            <!-- <p>Consectetur lorem ipsum dolor sit amet,  adipisicing elit.Lorem ipsum dolor, consectetur adipisicing elit.</p> -->
          </div>
      </div>
    </div>
    <div class="row">
     <div class="col-lg-12 col-md-12">
        <h4 class="theme-color">1. Refund Policy</h4>
        <p class="mb-20">Once the first payment installment has been received and the order confirmed, cancelling within the next 24hrs will lead to a full refund, 48hrs guarantees half refund while longer than 48hrs will attract no refund.</p>
        <p class="mb-20">The company may also cancel your order and pay back a full refund to you if there is no writer available to fulfill the order</p>
        <p class="mb-20">If the deadline is missed without prior contact from us asking you if it possible to extend the deadline, then a 10% refund will be issued for 1 day after the deadline, 20% for 2 days after the deadline and is missed by 3 days or more, a full refund will be issued.</p>

        <h4 class="theme-color mt-30">2. Fair Use Policy</h4>
        <p class="mb-20">Every work received from us are to be used as reference materials only and should not be submitted as your own work. We trust that you will respect this policy and use the service wisely.</p>

        <h4 class="theme-color mt-30"> 3. Plagiarism Free Service</h4>
        <p class="mb-30">We pass your order through plagiarism checkers like Grammarly to ensure that your assignment plagiarism count is at the acceptable level, usually 10% for a final year project and less for shorter report. Higher number of plagiarism will be amended on request for free.</p>

        <!-- <a class="button" href="#"><span>Accept</span></a>
        <a class="button" href="#"><span>Close</span></a> -->
     </div> 
    </div>
  </div>
</section>

<!--=================================
 terms-and-conditions -->
     
<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

</body>
</html> 