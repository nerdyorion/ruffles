

<div class="col-lg-9">

  <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block img-fluid" src="assets/images/banner1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner3.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <h3>FAQs</h3>

  <div class="row">
    <div id="accordion" class="col-md-12">
      <?php foreach($rows as $key => $row): ?>
        <div class="card">
          <div class="card-header" id="heading<?php echo $row['id']; ?>">
            <h5 class="mb-0">
              <button class="btn btn-link <?php echo $key == 0 ? '' : 'collapsed'; ?>" data-toggle="collapse" data-target="#collapse<?php echo $row['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $row['id']; ?>">
                <?php echo $row['question']; ?>
              </button>
            </h5>
          </div>

          <div id="collapse<?php echo $row['id']; ?>" class="<?php echo $key == 0 ? 'collapse show' : 'collapse'; ?>" aria-labelledby="heading<?php echo $row['id']; ?>" data-parent="#accordion">
            <div class="card-body"><?php echo $row['answer']; ?></div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>

</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>