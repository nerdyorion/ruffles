
<div class="col-lg-9">

  <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block img-fluid" src="assets/images/banner1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner3.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <h3>Reset Password</h3>
    <?php if($error_code == 0 && !empty($error)): ?>
      <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>


  <div class="row">


      <div class="col-md-6 col-md-offset-3">
      <div class="pb-50 clearfix">
        <?php echo form_open('Reset-Password/token/' . $token, 'class="form-horizontal", onsubmit="return validate();"'); ?>
      <div class="section-field mb-20">
       <label class="mb-10" for="password">Password* </label>
       <input id="password" name="password" class="Password form-control" type="password" placeholder="***********" required="required" />
     </div>
     <div class="section-field mb-20">
       <label class="mb-10" for="passconf">Confirm Password* </label>
       <input id="passconf" name="passconf" class="Password form-control" type="password" placeholder="***********" required="required" />
     </div>
     <div class="section-field">
      <div class="remember-checkbox mb-30">
                 <!-- <input type="checkbox" class="form-control" name="two" id="two" />
                   <label for="two"> Remember me</label> -->
                   <a href="login" class="pull-right">&larr; Login</a>
                 </div>
               </div>
               <button class="btn btn-primary" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Reset Password</span> <i class='fa fa-check'></i>">
                <span>Reset Password</span>
                <i class="fa fa-check"></i>
              </button> 
            </form>
          </div>
          <hr />
        </div>
    </div>

  </div>
  <!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
    function validate()
    {
      if($('#passconf').val() != $('#password').val())
      {
            // alert('Passwords don\'t match');
            $('#passconf').parent().addClass('has-error');
            return false;
          }
          $(':input[type="submit"]').prop('disabled', true);
          $('button[type="submit"]').prop('disabled', true);
          return true;
        }
</script>

</body>

</html>