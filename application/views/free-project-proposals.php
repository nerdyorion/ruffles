<style type="text/css">
.sidebar-widgets-wrap .recent-item .recent-title a {
  color: #73a02e;
  font-weight: normal;
}
.sidebar-widgets-wrap .recent-item .recent-title a:hover {
  color: #333;
  /*color: #84ba3f;*/
  /*font-weight: bold;*/
}
</style>
<style type="text/css">
.dropdown.dropdown-lg .dropdown-menu {
  margin-top: -1px;
  padding: 6px 20px;
}
.input-group-btn .btn-group {
  display: flex !important;
}
.btn-group .btn {
  border-radius: 0;
  margin-left: -1px;
}
.btn-group .btn:last-child {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
  margin-left: 0;
  margin-right: 0;
}
.form-group .form-control:last-child {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
  #adv-search {
    width: 500px;
    margin: 0 auto;
  }
  .dropdown.dropdown-lg {
    position: static !important;
  }
  .dropdown.dropdown-lg .dropdown-menu {
    min-width: 500px;
  }
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: auto;
  border: 3px solid #73AD21;
  z-index: 1;
</style>

<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Free Project Proposals</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Solution Library</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Free Project Proposals</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

 <!--=================================
   shop-06-sub-banner -->

   <a name="addedToCartAlert" id="addedToCartAlert"></a>
<!-- 
   <section class="shop-06-sub-banner black-bg pt-40 pb-40">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
            <div class="feature-icon">
              <span class="ti-loop text-white"></span>
            </div>
            <div class="feature-info">
              <h6 class="pt-15 text-white">Satisfaction Guaranteed</h6>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 sm-mb-30">
          <div class="feature-text left-icon">
           <div class="feature-icon">
            <span class="ti-gift text-white"></span>
          </div>
          <div class="feature-info">
            <h6 class="pt-15 text-white">Payment Secure</h6>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 xs-mb-30">
       <div class="feature-text left-icon">
         <div class="feature-icon">
          <span class="ti-user text-white"></span>
        </div>
        <div class="feature-info">
          <h6 class="pt-15 text-white">Online Support</h6>
        </div>
      </div>
    </div>
  </div>
</div>
</section> 
-->

<!--=================================
  shop-06-sub-banner -->


<!--=================================
  shop -->

  <section class="shop grid page-section-ptb  alerts-and-callouts">
    <div class="container">
      <div class="row">
        <div class="bs-callout bs-callout-success text-center hidden" style="margin-top:-40px;">
          <h4 id="addedToCartName">-</h4>
          &pound;<span id="addedToCartPrice">-</span> <br />
          added to Cart <br />
          <a class="button small mt-0" href="checkout">Checkout</a>
        </div>
        <div class="col-lg-3 col-md-3">
          <div class="sidebar-widgets-wrap">
           <div class="sidebar-widget mb-40">
            <h5 class="mb-20">search</h5>
            <div class="widget-search">
              <?php echo form_open('Free-Project-Proposals', 'class="form-horizontal", method="get", id="searchForm", onsubmit="return validate();"'); ?>
              <i class="fa fa-search" id="searchIcon"></i>
              <input type="search" id="title" name="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" class="form-control placeholder" placeholder="Search....">
            </form>
          </div>
        </div>
        <div class="sidebar-widget mb-40">
          <h5 class="mb-20">Level</h5>
          <div class="widget-link">
            <ul>
              <?php foreach ($levels as $row): ?>
                <li> <a href="Free-Project-Proposals?level_id=<?php echo $row['id']; ?>"> <i class="fa fa-angle-double-right"></i> <?php echo $row['name']; ?> </a></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
        <div class="sidebar-widget mb-40">
          <h5 class="mb-20">Recent items</h5>
          <?php foreach ($recent_items as $row): ?>
            <div class="recent-item clearfix">
              <div class="recent-info">
                <div class="recent-title">
                 <a href="Free-Project-Proposals?id=<?php echo $row['id']; ?>" class="text-success"><?php echo ellipsize($row['title'], 40); ?></a> 
               </div>
               <div class="recent-meta">
                 <ul class="list-style-unstyled">
                  <li class="color"><?php echo dashIfEmpty($row['level_name']); ?></li>
                </ul>    
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <div class="col-lg-9 col-md-9 sm-mt-40">
    <div class="row" style="padding-left: 30px;">
      <?php echo form_open('Free-Project-Proposals', 'class="form-inline", method="get", role="form"'); ?>
      <div class="form-group">
        <label for="level_id">Level:</label>
        <select class="wide  mb-30" id="level_id" name="level_id">
          <option value="0" selected="selected">All</option>
          <?php foreach ($levels_all as $level): ?>
            <option value="<?php echo $level['id']; ?>" <?php $level_id = isset($_GET['level_id']) ? trim($_GET['level_id']) : '-'; echo $level_id == $level['id'] ? 'selected="selected"' : ''; ?>><?php echo $level['name']; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <button type="submit" class="btn btn-default">Search</button>
    </form>
  </div>

  <div class="row">
    <div class="col-lg-12 text-center mt-40">
      <h6> Can't find your Free Project Proposal? <a class="theme-color" href="Request-Quote">Upload your past paper to get one of our experts solve it for you</a> </h6>
    </div>     
  </div>
  <?php if(empty($rows)): ?>
    <div class="row">
     <div class="product listing">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="product-des text-left">
          <div class="product-info mt-20">
            <p>... no record(s) found ...</p>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php else: ?>
  <?php foreach ($rows as $row): ?>
    <div class="row">
     <div class="product listing">
      <div class="col-lg-12 col-md-12 col-sm-12 mb-40">

        <div class="product-des text-left">
         <div class="product-title">
           <a href="Free-Project-Proposals/view/<?php echo $row['id']; ?>"><?php echo dashIfEmpty($row['title']); ?></a>
         </div>
         <div class="product-price">
          <!-- <ins><?php echo dashIfEmpty($row['level_name']); ?></ins> -->
          <i class="fa fa-university"></i> <?php echo dashIfEmpty($row['level_name']); ?>
        </div>
        <div class="product-info mt-20">
          <?php if(isset($_GET['title'])): ?>
            <?php 
            $words = explode(' ', trim($_GET['title'])); 
            
            $row['description'] = strlen(dashIfEmpty($row['description'])) > 300 ? ellipsize(dashIfEmpty($row['description']), 300) . ' <a href="Free-Project-Proposals/view/' . $row['id'] . '"><b>View&rarr;</b></a>': dashIfEmpty($row['description']);

            // var_dump($row['description']);  var_dump($words); die;

            $description = highlight_words( $row['description'], $words ); // var_dump($description); die;
            ?>
            <p><?php echo dashIfEmpty($description); ?></p>
          <?php else: ?>
            <p><?php echo strlen(dashIfEmpty($row['description'])) > 300 ? ellipsize(dashIfEmpty($row['description']), 300) . ' <a href="Free-Project-Proposals/view/' . $row['id'] . '"><b>View&rarr;</b></a>': dashIfEmpty($row['description']); ?></p>
          <?php endif; ?>
          <button class="button small mt-0" onclick="location.href='Free-Project-Proposals/download/<?php echo $row['id']; ?>';" style="display: inline-block;margin-bottom: 10px;">download</button>
          <button class="button small mt-0" onclick="location.href='Request-Quote';" style="display: inline-block;">Get Full Plagiarism Free Version</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
<?php endif; ?>
</div>
<div class=" pull-right">

  <nav aria-label="...">
    <ul class="pagination">
      <?php echo $links; ?>
    </ul>
  </nav>
</div>

<div class="row">
  <div class="col-lg-12 text-center mt-40">
    <h6> Can't find your Free Project Proposal? <a class="theme-color" href="Request-Quote">Upload your past paper to get one of our experts solve it for you</a> </h6>
  </div>     
</div>
</div>
</div>
</section>

<!--=================================
  welcome -->

<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    function validate ()
    {
      var title = document.getElementById("title").value;
      if(title == "" || title == null)
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    $(document).ready(function() {
      $('#searchIcon').click(function () {
        $('#searchForm').submit();
      });
    });
  </script>

</body>
</html> 