<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->
  <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

  <!-- <h6>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6> -->
<!--=================================
  side-nav-menu -->


  <h4 class="title"><u>Inquiry Details</u></h4>
  <!-- row -->
  <div class="row">
    <div class="col-md-12">
      <div class="white-box">
        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <tbody>
              <tr>
                <th>Question</th>
                <td><?php echo dashIfEmpty($row['question']); ?></td>
              </tr>
              <tr>
                <th>Reply</th>
                <td><?php echo is_null($row['reply']) || empty($row['reply']) ? '<mark style="background-color: #FF0800">PENDING</mark>' : ellipsize($row['reply'], 100); ?></td>
              </tr>
              <tr>
                <th>Date</th>
                <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
              </tr>
            </tbody>
          </table>
        </div>
        <p class="text-center">
          <button type="button" class="btn btn-primary waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
        </p>
      </div>
    </div>
  </div>
  <!-- /.row -->

</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>