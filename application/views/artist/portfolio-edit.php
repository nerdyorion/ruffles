  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Portfolio</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "artist/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "artist/"; ?>portfolio">Portfolio</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <?php if($row['type'] == 'image'): ?>
                <div class="col-md-12">
                  <?php echo form_open_multipart('artist/portfolio/edit/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image (650x650 px): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                          <span class="fileinput-new">Select file</span>
                          <span class="fileinput-exists">Change</span>
                          <input type="file" name="image" id="image"></span>
                          <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="image_url_old" value="<?php echo $row['image_url']; ?>" />
                    <input type="hidden" name="image_url_thumb_old" value="<?php echo image_thumb($row['image_url']); ?>" />
                    <div class="form-group">
                      <label for="description" class="col-sm-2 control-label">Description:</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group m-b-0">
                      <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      </div>
                    </div>
                  </form>
                </div>
                <?php elseif($row['type'] == 'audio'): ?>
                <div class="col-md-12">
                  <?php echo form_open_multipart('artist/portfolio/edit/' . $row['id'], 'class="form-horizontal"'); ?>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="source" class="col-sm-2 control-label">Source: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="source" id="source" required="required">
                          <option value="soundcloud" <?php echo $row['source'] == 'soundcloud' ? 'selected="selected"' : ''; ?>>SoundCloud</option>
                          <option value="youtube" <?php echo $row['source'] == 'youtube' ? 'selected="selected"' : ''; ?>>YouTube</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Audio ID: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="media_id" maxlength="50" id="media_id" value="<?php echo $row['media_id']; ?>" placeholder="" required="required">
                      <span class="help-block">E.g 338448041 in SoundCloud, KYXpl9iyrs0 in <a href="https://youtube.com/watch?v=KYXpl9iyrs0" target="_blank">https://youtube.com/watch?v=KYXpl9iyrs0</a></span>
                    </div>
                  </div>
                    <div class="form-group">
                      <label for="description" class="col-sm-2 control-label">Description:</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group m-b-0">
                      <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      </div>
                    </div>
                  </form>
                </div>
                <?php elseif($row['type'] == 'video'): ?>
                <div class="col-md-12">
                  <?php echo form_open_multipart('artist/portfolio/edit/' . $row['id'], 'class="form-horizontal"'); ?>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="20000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="source" class="col-sm-2 control-label">Source: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="source" id="source" required="required">
                          <option value="youtube" <?php echo $row['source'] == 'youtube' ? 'selected="selected"' : ''; ?>>YouTube</option>
                          <option value="vimeo" <?php echo $row['source'] == 'vimeo' ? 'selected="selected"' : ''; ?>>Vimeo</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Video ID: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="media_id" maxlength="50" id="media_id" value="<?php echo $row['media_id']; ?>" placeholder="" required="required">
                      <span class="help-block">E.g 247839331 in <a href="https://vimeo.com/247839331" target="_blank">https://vimeo.com/247839331</a>, KYXpl9iyrs0 in <a href="https://youtube.com/watch?v=KYXpl9iyrs0" target="_blank">https://youtube.com/watch?v=KYXpl9iyrs0</a></span>
                    </div>
                  </div>
                    <div class="form-group">
                      <label for="description" class="col-sm-2 control-label">Description:</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" name="description" id="description" rows="5"><?php echo $row['description']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group m-b-0">
                      <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      </div>
                    </div>
                  </form>
                </div>
                <?php endif; ?>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

  <?php $this->load->view($this->config->item('template_dir_artist') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
  <script src="assets/js/jasny-bootstrap.js"></script>
  <script type="text/javascript">
    function validate()
    {

      if(document.getElementById("image").files.length == 0 ){
      // no file uploaded
      return true;
    }
    else {
      var fileName = $("#image").val();
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
</script>

</body>
</html>