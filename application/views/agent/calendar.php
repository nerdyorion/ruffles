  <link href="assets/css/calendar.css" rel="stylesheet">
  <style type="text/css">
    table.table.table-bordered tbody tr td{
       vertical-align: middle;
    }
  </style>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Calendar</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "agent/"; ?>">Dashboard</a></li>
            <li class="active">Calendar</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
          <?php echo $calendar; ?></p>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
          <div class="white-box">
            <!--<h3>Products</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs" id="selected-date"> Event(s) for today</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Event</th>
                          <th>Status</th>
                          <th>Time</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="4" align="center" id="sn">You didn't book any artist for an event this day.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td id="sn"><?php echo $sn++; ?></td>
                          <td title="<?php echo $row['event_name']; ?>"><?php echo $row['event_name']; ?></td>
                          <td>
                            <?php echo $row['accepted'] == 1 ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Accepted&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] == 1 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Rejected&nbsp;</a>' : ''; ?> 
                            <?php echo $row['rejected'] != 1 && $row['paid'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;Unpaid&nbsp;</a>' : ''; ?>
                          </td>
                          <td title="<?php echo $row['event_start_time'] . '-' . $row['event_end_time']; ?>"><?php echo $row['event_start_time'] . '-' . $row['event_end_time']; ?></td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- <p><?php //echo $links; ?></p> -->
                </div>
                
                <div class="col-md-3 pull-right pagination">
                  <p><?php echo $links; ?></p>
                </div>
                
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_agent') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n"; ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/moment.js"></script>
<script type="text/javascript">
  $(".calendar_days, .calendar_today").click(function(){
    // alert($(this).data("date"));
    // $("#selected-date").html("Event(s) for " + moment('$(this).data("date")', "YYYY-MM-DD"));
    $("#selected-date").html("Event(s) for " + $(this).data("date"));
    $.ajax({
      type: 'GET',
      url: '<?php echo base_url(); ?>agent/bookings/byDate/' + $(this).data("date"),
      success: function( data ) {
        if(data == '')
        {
          var default_msg = '<tr><td colspan="4" align="center" id="sn">You didn\'t book any artist for an event this day.</td></tr>';
          $("td#sn").parent().replaceWith(default_msg);
        }
        else
        {
          $("td#sn").parent().replaceWith(data);
        }
      },
      error: function(xhr, status, error) {
        // check status && error
        // console.log(status);
        // console.log(error);
      },
      dataType: 'text'
    });
  });
</script>

</body>
</html>