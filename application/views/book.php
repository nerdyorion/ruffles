  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li> -->
    </ol>

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <!-- <img src="assets/images/banner.png" alt="Image"> -->
        <img src="assets/images/register-banner.png" alt="Image" />
        <div class="carousel-caption">
          <h3 style="color: #f9f9f9;">Book <?php echo '<em>' . $row['stage_name'] . '</em>'; ?></h3>
          <p style="color: #f9f9f9;">...</p>
        </div>      
      </div>

    </div>
  </div>

  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->

  <div class="container marketing">

    <!-- Three columns of text below the carousel -->
    <div class="row" align="center">
      <div class="col-lg-6 col-lg-offset-3 text-center">
        <h2>Booking Details</h2>

        <hr />

        <?php if($error_code == 0 && !empty($error)): ?>
          <div class="alert alert-success alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $error; ?>
          </div>
        <?php elseif($error_code == 1 && !empty($error)): ?>
          <div class="alert alert-danger alert-dismissable fade in">
            <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Error!</strong> <?php echo $error; ?>
          </div>
        <?php else: ?>
        <?php endif; ?>

        <div class="clearfix"></div>

        <?php if((int) $row["category_social_influencer_id"] == 1): ?> <!-- 1 = Content Creator -->
          <?php echo form_open('/book/secureCC/' . $row["username"], 'class="form-horizontal", onsubmit="return validate();"'); ?>
            <input type="hidden" name="talent_id" value="<?php echo $row['id']; ?>">
            <div class="form-group">
          <label class="col-sm-3 control-label">Artist: <span class="required">*</span></label>
          <div class="col-sm-9 text-left">
            <b><?php echo $row['stage_name']; ?></b>
          </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
          <label for="content_brief" class="col-sm-3 control-label">Brief: <span class="required">*</span></label>
          <div class="col-sm-9">
            <textarea class="form-control" name="content_brief" id="content_brief" required="required"></textarea>
          </div>
            </div>
            <div class="form-group">
          <label for="content_industry" class="col-sm-3 control-label">Industry: <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <select class="form-control" name="content_industry" id="content_industry" required="required">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($content_industry as $row): ?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['value']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
            </div>
            <div class="form-group">
          <label class="col-sm-3 control-label">Refundable Booking Fee: <span class="required">*</span></label>
          <div class="col-sm-9 text-left">
            <b>N XXXXXXXX</b>
          </div>
            </div>
            <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit Booking Request</button>
          </div>
            </div>
          </form>
        <?php elseif((int) $row["category_social_influencer_id"] == 2): ?> <!-- 2 = Amplifier -->
          <?php echo form_open('/book/secureA/' . $row["username"], 'class="form-horizontal", onsubmit="return validate();"'); ?>
            <input type="hidden" name="talent_id" value="<?php echo $row['id']; ?>">
            <div class="form-group">
          <label class="col-sm-3 control-label">Artist: <span class="required">*</span></label>
          <div class="col-sm-9 text-left">
            <b><?php echo $row['stage_name']; ?></b>
          </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
          <label for="content_industry" class="col-sm-3 control-label">Industry: <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <select class="form-control" name="content_industry" id="content_industry" required="required">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($content_industry as $row): ?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['value']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
            </div>
            <div class="form-group">
          <label for="content_budget" class="col-sm-3 control-label">Budget: <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <select class="form-control" name="content_budget" id="content_budget" required="required">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($content_budget as $row): ?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['value']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
            </div>
            <div class="form-group">
          <label for="content_tier" class="col-sm-3 control-label">Tier: <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <select class="form-control" name="content_tier" id="content_tier" required="required">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($content_tier as $row): ?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['value']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
            </div>
            <div class="form-group">
          <label for="content_campaign_duration" class="col-sm-3 control-label">Campaign Duration: <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <select class="form-control" name="content_campaign_duration" id="content_campaign_duration" required="required">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($content_campaign_duration as $row): ?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['value']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
            </div>
            <div class="form-group">
          <label class="col-sm-3 control-label">Refundable Booking Fee: <span class="required">*</span></label>
          <div class="col-sm-9 text-left">
            <b>N XXXXXXXX</b>
          </div>
            </div>
            <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit Booking Request</button>
          </div>
            </div>
          </form>
        <?php else: ?> <!-- 0 = NOT A SOCIAL INFLUENCER -->
          <?php echo form_open('/book/secure/' . $row["username"], 'class="form-horizontal", onsubmit="return validate();"'); ?>
            <input type="hidden" name="talent_id" value="<?php echo $row['id']; ?>">
            <div class="form-group">
          <label class="col-sm-3 control-label">Artist: <span class="required">*</span></label>
          <div class="col-sm-9 text-left">
            <b><?php echo $row['stage_name']; ?></b>
          </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
          <label for="event_name" class="col-sm-3 control-label">Event Name: <span class="required">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="event_name" id="event_name" placeholder="Wedding" maxlength="2000" required="required" />
          </div>
            </div>
            <div class="form-group">
          <label for="event_date" class="col-sm-3 control-label">Event Date: <span class="required">*</span></label>
          <div class="col-sm-9">
            <input type="date" class="form-control" name="event_date" id="event_date" maxlength="255" required="required" />
          </div>
            </div>
            <div class="form-group">
          <label for="event_start_time" class="col-sm-3 control-label">Start Time: <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <select class="form-control" name="event_start_time" id="event_start_time" required="required">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($event_time as $item): ?>
                <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
            </div>
            <div class="form-group">
          <label for="event_end_time" class="col-sm-3 control-label">End Time: <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <select class="form-control" name="event_end_time" id="event_end_time" required="required">
              <option value="0" selected="selected">-- Select --</option>
              <?php foreach ($event_time as $item): ?>
                <option value="<?php echo $item; ?>"><?php echo $item; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
            </div>
            <div class="form-group">
          <label for="event_location" class="col-sm-3 control-label">Event Location: <span class="required">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="event_location" id="event_location" placeholder="Civic Centre" maxlength="255" required="required" />
          </div>
            </div>
            <div class="form-group">
          <label for="event_description" class="col-sm-3 control-label">Event Description: <span class="required">*</span></label>
          <div class="col-sm-9">
            <textarea class="form-control" name="event_description" id="event_description" required="required"></textarea>
          </div>
            </div>
            <div class="form-group">
          <label class="col-sm-3 control-label">Refundable Booking Fee: <span class="required">*</span></label>
          <div class="col-sm-9 text-left">
            <b>N XXXXXXXX</b>
          </div>
            </div>
            <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit Booking Request</button>
          </div>
            </div>
          </form>
      <?php endif; ?>
    </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->

  <?php $this->load->view('footer'); echo "\n"; ?>
  <script type="text/javascript">
    function validate()
    {
      var event_start_time = document.getElementById("event_start_time").value;
      var event_end_time = document.getElementById("event_end_time").value;
      if(event_end_time == 0 ){
        $('#event_end_time').parent().parent().addClass('has-error');

        // clear others
        $('#event_start_time').parent().parent().removeClass('has-error');

        return false;
      }
      else if(event_start_time == 0 ){
        $('#event_start_time').parent().parent().addClass('has-error');

        // clear others
        $('#event_end_time').parent().parent().removeClass('has-error');

        return false;
      }
      else {
        $(':input[type="submit"]').prop('disabled', true);
        return true;
      }
    }
  </script>

    </div><!-- /.container -->

  </body>
  </html>