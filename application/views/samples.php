
<!--=================================
  page-title-->

  <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(assets/images/bg/bg1.jpg);">
    <div class="container">
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="page-title-name">
            <h1>Samples</h1>
            <p>We know the secret of your success</p>
          </div>
          <ul class="page-breadcrumb">
            <li><a href="./"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><a href="#">Company</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Samples</span> </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<!--=================================
  page-title -->

  
 <!--=================================
   team -->
   
   <section class="page-section-ptb">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- <h4 class="mb-30">Team Default</h4> -->
          <div class="section-title text-center">
            <h6>Samples of work</h6>
            <h2 class="title-effect">see for yourself</h2>
            <p>Take Your Success To New Heights With AcademicianHelp.</p>
          </div>
        </div>
      </div>
      <div class="row mt-30">
        <div class="col-lg-12">
         <h4 class="mb-30">Dissertation</h4>
       </div>
       <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Dissertation - An investigation into the integration of Three E-learning Open Source Software">
        <a href="assets/sample-docs/dissertation/Dissertation - An investigation into the integration of Three E-learning Open SOurce Software.pdf" download>
         <div class="team team-hover team-overlay text-center">
          <div class="team-photo">
            <img class="img-responsive center-block" src="assets/images/samples/open-source.png" alt=""> 
          </div>    
          <div class="team-description"> 
            <div class="team-info"> 
             <h5 class="text-white">An Investigation into the i...</h5>
             <span class="text-white">DOWNLOAD</span>
           </div>
         </div>
       </div>
     </a>
   </div>
   <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Dissertation - An Investigation into the use of Data Mining tools to aid forensic investigations">
    <a href="assets/sample-docs/dissertation/Dissertation - An Investigation into the use of Data Mining tools to aid forensic investigations.pdf" download>
     <div class="team team-hover team-overlay text-center">
      <div class="team-photo">
        <img class="img-responsive center-block" src="assets/images/samples/forensic.jpg" alt=""> 
      </div>
      <div class="team-description"> 
        <div class="team-info"> 
         <h5 class="text-white">An Investigation into the u...</h5>
         <span class="text-white">DOWNLOAD</span>
       </div>
     </div>
   </div>
 </a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Dissertation - Impact of Cybercrime in Saudi Arabia">
  <a href="assets/sample-docs/dissertation/Dissertation - Impact of Cybercrime in Saudi Arabia.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/cyber-crime.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
        <h5 class="text-white">Impact of Cybercrime i...</h5>
        <span class="text-white">DOWNLOAD</span>
      </div>
    </div>
  </div>
</a>
</div>  
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Dissertation - Investigation of different VPN Solutions for Mobile Network">
  <a href="assets/sample-docs/dissertation/Dissertation - Investigation of different VPN Solutions for Mobile Network.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/vpn.png" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Investigation of different VPN...</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
</div>   
<div class="row mt-20">
 <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Dissertation - Smart Car park System">
  <a href="assets/sample-docs/dissertation/Dissertation - Smart Car park System.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/smart-car-park.png" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Smart Car park System</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Dissertation - Spam Detection on Twitter">
  <a href="assets/sample-docs/dissertation/Dissertation - Spam Detection on Twitter.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/spam.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Spam Detection on Twitter</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Dissertation - Student Accommodation System">
  <a href="assets/sample-docs/dissertation/Dissertation - Student Accommodation System.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/accomodation.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Student Accommodation System</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
</div>   


<div class="row mt-60">
  <div class="col-lg-12">
   <h4 class="mb-30">Management</h4>
 </div>
 <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Management - Essay">
  <a href="assets/sample-docs/management/Management - Essay.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/essay.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Essay</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Management - Portfolio">
  <a href="assets/sample-docs/management/Management - Portfolio.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/portfolio.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Portfolio </h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Management - Reflection">
  <a href="assets/sample-docs/management/Management - Reflection.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/reflection.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Reflection</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
</div>  

<div class="row mt-60">
  <div class="col-lg-12">
   <h4 class="mb-30">Law</h4>
 </div>
 <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Law - Abstract">
  <a href="assets/sample-docs/law/Law - Abstract.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/abstract.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Abstract</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Law - Book Review">
  <a href="assets/sample-docs/law/Law - Book Review.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/review.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Book Review </h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Law - Criminal Law Essay">
  <a href="assets/sample-docs/law/Law - Criminal Law Essay.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/criminal.png" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Criminal Law Essay</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>  
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Law - Editorial">
  <a href="assets/sample-docs/law/Law - Editorial.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/editorial.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Editorial</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</div>
</a>
</div>   

<div class="row mt-20">
 <div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Law - Law Reform Proposal">
  <a href="assets/sample-docs/law/Law - Law Reform Proposal.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/reform.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Law Reform Proposal</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 xs-mb-30 sample-box" title="Law - Skeleton Argument for Junior Counsel for The Respondent">
  <a href="assets/sample-docs/law/Law - SKELETON ARGUMENT FOR JUNIOR COUNSEL FOR THE RESPONDENT.pdf" download>
   <div class="team team-hover team-overlay text-center">
    <div class="team-photo">
      <img class="img-responsive center-block" src="assets/images/samples/argument.jpg" alt=""> 
    </div>    
    <div class="team-description"> 
      <div class="team-info"> 
       <h5 class="text-white">Skeleton Argument ...</h5>
       <span class="text-white">DOWNLOAD</span>
     </div>
   </div>
 </div>
</a>
</div>
</div>  
</div>
</section>

<!--=================================
 team -->


<!--=================================
  action box- -->

  <section class="action-box theme-bg full-width">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <h3><strong> Academician Help </strong></h3>
        <p>Your one-stop for all tutoring, writing, editing and proof-reading needs.</p>
        <a class="button border white" href="Request-Quote">
          <span>Get Quote</span>
          <i class="fa fa-sign-in"></i>
        </a> 
      </div>
    </div>
  </div>
</section>

<!--=================================
  action box- -->

  <?php $this->load->view('footer'); echo "\n"; ?>

  <script type="text/javascript">
    $(document).ready(function (){
      $('.sample-box').click(function () {
        // alert('hi');
      });
    });
  </script>

</body>
</html> 