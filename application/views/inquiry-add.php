<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->
    <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

    <!-- <h6>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6> -->
<!--=================================
  side-nav-menu -->

  <h4 class="title"><u>Make Inquiry</u></h4>
  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>



  <div class="row">
  <div class="col-lg-12 col-md-12">

    <?php echo form_open('/inquiries/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
    <div class="section-field mb-20">
      <label class="mb-10" for="question">Question </label>
      <textarea class="web form-control" name="question" id="question" maxlength="8000"></textarea>
    </div>
    <button class="btn btn-primary" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Submit</span> <i class='fa fa-check'></i>">
      <span>Submit</span>
      <i class="fa fa-check"></i>
    </button> 
  </form>

</div>
</div>
</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {
    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>