
<div class="col-lg-9">

  <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img class="d-block img-fluid" src="assets/images/banner1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block img-fluid" src="assets/images/banner3.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  
  <div class="bs-callout bs-callout-success text-center hidden d-none" style="margin-top:20px;">
    <h4 id="addedToCartName">-</h4>
    &pound;<span id="addedToCartPrice">-</span> <br />
    added to Cart <br />
    <a class="btn btn-primary" href="checkout">Checkout</a>
  </div>
  <h3>Hot Offers</h3>

  <a name="addedToCartAlert" id="addedToCartAlert"></a>
  <div class="row">
    <?php if(empty($hot_offers)): ?>
      <div class="col-lg-12 col-md-12 mb-12 text-center">
        <p>... we are baking :) please check back later ...</p>
      </div>
    <?php else: ?>
      <?php foreach($hot_offers as $row): ?>
        <div class="col-lg-4 col-md-6 mb-4">
          <div class="card h-100">
            <a href="<?php echo base_url() . 'products/view/' . $row['id']; ?>">
              <div style="min-height: 250px; border: 1px solid #afafaf;">
                <img class="card-img-top img-responsive" src="assets/images/products/<?php echo $row['image_url']; ?>" alt="">
              </div>
            </a>
            <div class="card-body">
              <h6 class="card-title">
                <a href="<?php echo base_url() . 'products/view/' . $row['id']; ?>"><?php echo $row['name']; ?></a>
              </h6>
              <h5>&pound;<?php echo number_format($row['price']); ?></h5>
              <p class="card-text"><?php echo ellipsize($row['description'], 50); ?></p>
            </div>
            <div class="card-footer">
              <?php if(is_logged_in() && !is_super_admin()): ?>
                <button class="btn btn-primary add-to-cart" href="javascript:void(0);" data-id="<?php echo $row['id']; ?>" data-price="<?php echo $row['price']; ?>" data-name="<?php echo $row['name']; ?>" data-image="<?php echo $row['image_url']; ?>" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Add to Cart">Add to Cart</button>
              <?php else: ?>
                <a class="btn btn-primary" href="register">Add to Cart</a>
              <?php endif; ?>
              <a class="btn btn-primary" href="products/view/<?php echo $row['id']; ?>">View</a>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>

  </div>

  <h3>Featured Products</h3>

  <div class="row">
    <?php if(empty($featured)): ?>
      <div class="col-lg-12 col-md-12 mb-12 text-center">
        <p>... we are baking :) please check back later ...</p>
      </div>
    <?php else: ?>
      <?php foreach($featured as $row): ?>
        <div class="col-lg-4 col-md-6 mb-4">
          <div class="card h-100">
            <a href="<?php echo base_url() . 'products/view/' . $row['id']; ?>">
              <div style="min-height: 250px; border: 1px solid #afafaf;">
                <img class="card-img-top img-responsive" src="assets/images/products/<?php echo $row['image_url']; ?>" alt="">
              </div>
            </a>
            <div class="card-body">
              <h6 class="card-title">
                <a href="<?php echo base_url() . 'products/view/' . $row['id']; ?>"><?php echo $row['name']; ?></a>
              </h6>
              <h5>&pound;<?php echo number_format($row['price']); ?></h5>
              <p class="card-text"><?php echo ellipsize($row['description'], 50); ?></p>
            </div>
            <div class="card-footer">
              <?php if(is_logged_in() && !is_super_admin()): ?>
                <button class="btn btn-primary add-to-cart" href="javascript:void(0);" data-id="<?php echo $row['id']; ?>" data-price="<?php echo $row['price']; ?>" data-name="<?php echo $row['name']; ?>" data-image="<?php echo $row['image_url']; ?>" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Add to Cart">Add to Cart</button>
              <?php else: ?>
                <a class="btn btn-primary" href="register">Add to Cart</a>
              <?php endif; ?>
              <a class="btn btn-primary" href="products/view/<?php echo $row['id']; ?>">View</a>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>

  </div>
  <!-- /.row -->

</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

</body>

</html>