    <!-- Footer -->
    <footer class="py-5 bg-dark" style="margin-top: 30px;">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ruffles <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span>
        <span class="pull-right"><a href="https://www.instagram.com/dee_truffles/" target="_blank" style="color: #ffffff;"><i class="fa fa-instagram"></i></a></span>
      </p>
      </div>
      <!-- /.container -->
    </footer>




    <!-- custom -->
    <script type="text/javascript" src="assets/js/cart.js"></script>
    <script type="text/javascript">
//       (function(){
 
//   $("#cart").on("click", function() {
//     $(".shopping-cart").fadeToggle( "fast");
//   });
  
// })();
    </script>