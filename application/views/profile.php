<div class="col-lg-9" style="margin-top: 30px;">

  <!-- <h3>Register</h3> -->
    <?php $this->load->view('side-nav-menu'); echo "\n"; ?>

    <!-- <h6>Welcome <?php echo isset($_SESSION['user_full_name']) ? explode(" ", $_SESSION['user_full_name'])[0] : "User"?>,</h6> -->
<!--=================================
  side-nav-menu -->

  <h4 class="title"><u>Profile</u></h4>
  <?php if($error_code == 0 && !empty($error)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color: #ffffff">
      <!-- <div class="alert alert-success alert-dismissable fade in"> -->
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $error; ?>
      </div>
    <?php elseif($error_code == 1 && !empty($error)): ?>
      <div class="alert alert-danger alert-dismissable" style="background-color: #ffffff">
        <a href="<?php echo current_url();?>#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?php echo $error; ?>
      </div>
    <?php else: ?>
    <?php endif; ?>



  <div class="row">
  <div class="col-lg-12 col-md-12">

    <?php echo form_open('/profile', 'class="form-horizontal", onsubmit="return validate();"'); ?>
    <div class="section-field mb-20">
      <label class="mb-10" for="first_name">First name* </label>
      <input id="first_name" name="first_name" class="web form-control" type="text" placeholder="e.g john" required="required" value="<?php echo $row['first_name']; ?>" />
    </div>
    <div class="section-field mb-20">
      <label class="mb-10" for="last_name">Last name* </label>
      <input id="last_name" name="last_name" class="web form-control" type="text" placeholder="e.g doe" required="required" value="<?php echo $row['last_name']; ?>" />
    </div>
    <div class="section-field mb-20">
      <label class="mb-10">Gender </label>
      <div class="box">
        <select class="form-control" id="gender" name="gender">
          <option value="0">-- Select --</option>
          <option value="Male" <?php echo strtolower($row['gender']) == "male" ? 'selected="selected"' : ''; ?>>Male</option>
          <option value="Female" <?php echo strtolower($row['gender']) == "female" ? 'selected="selected"' : ''; ?>>Female</option>
        </select>
      </div>
    </div>
    <div class="section-field mb-20">
      <label class="mb-10" for="phone">Phone </label>
      <input id="phone" name="phone" class="form-control" type="text" placeholder="e.g +4412345" value="<?php echo $row['phone']; ?>" />
    </div>
    <div class="section-field mb-20">
      <label class="mb-10" for="address">Address </label>
      <textarea class="web form-control" name="address" id="address" maxlength="8000"><?php echo $row['address']; ?></textarea>
    </div>
    <div class="section-field mb-20">
      <label class="mb-10">Country </label>
      <div class="box">
        <select class="form-control" id="country_id" name="country_id">
          <option value="0" selected="selected">-- Select --</option>
          <?php foreach ($countries as $country): ?>
            <option value="<?php echo $country['id']; ?>" <?php echo $country['id'] == $row['country_id'] ? 'selected="selected"' : ''; ?>><?php echo $country['name']; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <button class="btn btn-primary" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> <span>Update Profile</span> <i class='fa fa-check'></i>">
      <span>Update Profile</span>
      <i class="fa fa-check"></i>
    </button> 
  </form>

</div>
</div>
</div>
<!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->

<?php $this->load->view('footer'); echo "\n"; ?>

<script type="text/javascript">
  function validate()
  {

    if($('#first_name').val() != '' && $('#last_name').val() != '' && $('#email').val() != '' && $('#password').val() != '' )
    {
      // $('#cover-spin').show();
    }

    $('.hide-loader').click(function () {
      // $('#cover-spin').hide();
    });

    var $btn = $('button[type="submit"]').button('loading');
    $(':input[type="submit"]').prop('disabled', true);
    $('button[type="submit"]').prop('disabled', true);
    return true;
  }
</script>

</body>

</html>