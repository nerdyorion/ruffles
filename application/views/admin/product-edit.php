
<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update Product</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url() . "admin123/"; ?>products">Products</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/products/edit/' . $row['id'], 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="category_id" class="col-sm-2 control-label">Category: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="category_id" id="category_id" required="required">
                        <?php foreach ($categories as $category): ?>
                        <option value="<?php echo $category['id']; ?>" <?php echo $category['id'] == $row['category_id'] ? 'selected="selected"' : ''; ?>><?php echo $category['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Product Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="8000" id="name" value="<?php echo $row['name']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image (650x650 px): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="image_url_old" value="<?php echo $row['image_url']; ?>" />
                  <input type="hidden" name="image_url_thumb_old" value="<?php echo $row['image_url_thumb']; ?>" />
                  <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price (&pound;): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="number" min="0" class="form-control" name="price" maxlength="13" id="price" value="<?php echo $row['price']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="stock" class="col-sm-2 control-label">Stock (Qty): <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <input type="number" min="0" class="form-control" name="stock" maxlength="13" id="stock" value="<?php echo $row['count_stock']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" maxlength="20000" rows="5"><?php echo $row['description']; ?></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="manufactured_date" class="col-sm-2 control-label">Man. Date: <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <input type="text" data-date-format="yyyy-mm-dd" maxlength="10" class="form-control" name="manufactured_date" id="manufactured_date" value="<?php echo $row['manufactured_date']; ?>" placeholder="" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="expiry_date" class="col-sm-2 control-label">Exp. Date: <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <input type="text" data-date-format="yyyy-mm-dd" maxlength="10"  class="form-control" name="expiry_date" id="expiry_date" value="<?php echo $row['expiry_date']; ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Cancel</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  function validate()
  {
    var category_id = document.getElementById("category_id").value;
    
    if(category_id == 0 ){
      alert('Please specify category.');
      return false;
    }
    else if(document.getElementById("image").files.length == 0 ){
      // no file uploaded
      return true;
    }
    else {
      var fileName = $("#image").val(); // alert('fileName: ' + fileName + "\n" + typeof(fileName));
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3) || (fileName.lastIndexOf("bmp")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
  
  $('#manufactured_date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });
  
  $('#expiry_date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });
</script>

</body>
</html>