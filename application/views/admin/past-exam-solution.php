  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Past Exam Solution</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Past Exam Solution</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Past Exam Solution</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="<?php echo current_url(); ?>#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="<?php echo current_url(); ?>#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/Past-Exam-Solution', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="university_id">University:</label>
                      <select class="wide form-control mb-30" id="university_id" name="university_id">
                        <option value="0" selected="selected">All</option>
                        <?php foreach ($universities_all as $university): ?>
                          <option value="<?php echo $university['id']; ?>" <?php $university_id = isset($_GET['university_id']) ? trim($_GET['university_id']) : '-'; echo $university_id == $university['id'] ? 'selected="selected"' : ''; ?>><?php echo $university['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>University</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Price</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="4" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td><?php echo dashIfEmpty($row['university_name']); ?></td>
                          <td><?php echo ellipsize(dashIfEmpty($row['title']), 50); ?></td>
                          <td><?php echo ellipsize(dashIfEmpty($row['description']), 50); ?></td>
                          <td><?php echo dashIfEmpty('&pound;' . number_format($row['price'])); ?></td>
                          <td class="text-nowrap">
                            <a href="admin123/Past-Exam-Solution/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                            <a href="admin123/Past-Exam-Solution/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <a href="admin123/Past-Exam-Solution/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                            <a href="admin123/Past-Exam-Solution/download/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Download" target="_blank"> <i class="fa fa-download text-inverse m-r-10"></i> </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                -->
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/Past-Exam-Solution/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="university_id" class="col-sm-2 control-label">University: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="university_id" id="university_id" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($universities as $row): ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="title" maxlength="8000" id="title" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price(&pound;): <span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                      <input type="number" class="form-control" name="price" min="0" id="price" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="file_url" class="col-sm-2 control-label">File: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="file_url" id="file_url"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" maxlength="800000" rows="5"></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3 pull-right pagination">
              <p><?php echo $links; ?></p>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
    function validate()
    {
      var university_id = document.getElementById("university_id").value;
      var fileName = $("#file_url").val();

      if(university_id == 0 ){
        alert('Please specify university.');
        return false;
      }
      else if(document.getElementById("file_url").files.length == 0 ){
        alert('Please upload file.');
        return false;
      }
      else
      {
      }

      $(':input[type="submit"]').prop('disabled', true);
      $('button[type="submit"]').prop('disabled', true);
      return true;
    }
  </script>

</body>
</html>