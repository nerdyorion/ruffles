
<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
  <style type="text/css">
    table.table.table-bordered tbody tr td{
       vertical-align: middle;
    }
  </style>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Products</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Products</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Products</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <!-- Search Filter -->
                  <div class="row">
                    <?php echo form_open('/admin123/products', 'class="form-inline", method="get", role="form"'); ?>
                    <div class="form-group">
                      <label for="title">Search:</label>
                      <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="category_id">Category:</label>
                      <select class="wide form-control mb-30" name="category_id">
                        <option value="0" selected="selected">All</option>
                        <?php foreach ($categories_all as $category): ?>
                          <option value="<?php echo $category['id']; ?>" <?php $category_id = isset($_GET['category_id']) ? trim($_GET['category_id']) : '-'; echo $category_id == $category['id'] ? 'selected="selected"' : ''; ?>><?php echo $category['name']; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th></th>
                          <th>Product Name</th>
                          <th>Category</th>
                          <th>Price (&pound;)</th>
                          <th>Stock</th>
                          <th>Dates</th>
                          <th>Featured</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="9" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                      <?php foreach ($rows as $row): ?>
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td><div class="media">
                            <div class="media-left">
                              <a href="assets/images/products/<?php echo $row['image_url']; ?>" target="_blank">
                              <img class="media-object" src="assets/images/products/<?php echo $row['image_url_thumb']; ?>" alt="<?php echo $row['name']; ?>">
                              </a>
                            </div>
                          </div></td>
                          <td><?php echo $row['name']; ?></td>
                          <td><?php echo $row['category_name']; ?></td>
                          <td><?php echo number_format($row['price']); ?></td>
                          <td><?php echo (int) $row['count_stock'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;0&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;' . number_format($row['count_stock']). '&nbsp;</a>'; ?></td>
                          <td>
                            <small><b>Man. Date</b>: <?php echo $row['manufactured_date']; ?></small><br />
                            <small><b>Exp. Date</b>: <?php echo $row['expiry_date']; ?></small>
                          </td>
                          <td><?php echo (int) $row['featured'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td>
                          <td class="text-nowrap">
                            <a href="admin123/products/edit/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                            <a href="admin123/products/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-trash text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> 
                            <?php if($row['featured'] == 0): ?>
                              <a href="admin123/products/feature/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Set as Featured"> <i class="fa fa-trophy text-inverse m-r-10"></i> </a> 
                            <?php else: ?>
                              <a href="admin123/products/unfeature/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Remove Featured"> <i class="fa fa-trophy fa-rotate-90 text-inverse m-r-10"></i> </a> 
                            <?php endif; ?>
                            <a href="admin123/products/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                            <a href="<?php echo base_url() . 'products/' . $row['id']; ?>" data-toggle="tooltip" data-original-title="View in Store Front" target="_blank"> <i class="fa fa-external-link text-inverse m-r-10"></i> </a> 
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- <p><?php //echo $links; ?></p> -->
                </div>
                
                <div class="col-md-3 pull-right pagination">
                  <p><?php echo $links; ?></p>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <?php echo form_open_multipart('admin123/products/create', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                  <div class="form-group">
                    <label for="category_id" class="col-sm-2 control-label">Category: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="category_id" id="category_id" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <?php foreach ($categories as $row): ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                        </tr>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Product Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="8000" id="name" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Image (650x650 px): <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                          <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image" required="required"></span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price (&pound;): <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <input type="number" min="0" class="form-control" name="price" maxlength="13" id="price" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="stock" class="col-sm-2 control-label">Stock (Qty): <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <input type="number" min="0" class="form-control" name="stock" maxlength="13" id="stock" value="" placeholder="" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-6">
                      <textarea class="form-control" name="description" id="description" maxlength="20000" rows="2"></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="manufactured_date" class="col-sm-2 control-label">Manufac. Date: <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <input type="text" data-date-format="yyyy-mm-dd" maxlength="10" class="form-control" name="manufactured_date" id="manufactured_date" value="" placeholder="" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="expiry_date" class="col-sm-2 control-label">Expiry Date: <span class="text-danger">*</span></label>
                    <div class="col-sm-6">
                      <input type="text" data-date-format="yyyy-mm-dd" maxlength="10"  class="form-control" name="expiry_date" id="expiry_date" value="" placeholder="" required="required">
                    </div>
                  </div>

                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  function validate()
  {
    var category_id = document.getElementById("category_id").value; console.log(category_id); console.log(typeof(category_id));
    
    if(category_id == '0' ){
      alert('Please specify category.');
      return false;
    }
    else if(document.getElementById("image").files.length == 0 ){
      alert('Please upload image.');
      return false;
    }
    else {
      var fileName = $("#image").val();
      fileName = fileName.toLowerCase();
      
      if((fileName.lastIndexOf("jpg")===fileName.length-3) || (fileName.lastIndexOf("jpeg")===fileName.length-4) || (fileName.lastIndexOf("png")===fileName.length-3) || (fileName.lastIndexOf("gif")===fileName.length-3) || (fileName.lastIndexOf("bmp")===fileName.length-3))
      {
        //alert("OK");
        return true;
      }
      else
      {
        alert("Please upload valid image file.");
        return false;
      }
    }
  }
  
  $('#manufactured_date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });
  
  $('#expiry_date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });
</script>

</body>
</html>