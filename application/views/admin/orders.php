<!-- Custom CSS -->
<link href="assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-12">
        <h4 class="page-title">Orders</h4>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
          <li class="active">Orders</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="white-box">
          <!--<h3>Student</h3>-->
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all"> 
              <div class="col-md-12">
                <!-- Search Filter -->
                <div class="row">
                  <?php echo form_open('/admin123/orders', 'class="form-inline", method="get", role="form"'); ?>
                  <div class="form-group">
                    <label for="title">Search:</label>
                    <input type="text" class="form-control" name="title" maxlength="2000" id="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" placeholder="" />
                  </select>
                </div>
                <div class="form-group">
                  <label for="status_id">Status:</label>
                  <select class="wide form-control mb-30" id="status_id" name="status_id">
                    <option value="-1" selected="selected">All</option>
                    <?php foreach ($status_all as $status): ?>
                      <option value="<?php echo $status['id']; ?>" <?php $status_id = isset($_GET['status_id']) ? trim($_GET['status_id']) : '-'; echo $status_id == $status['id'] ? 'selected="selected"' : ''; ?>><?php echo $status['name']; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="date">Date:</label>
                  <div class="input-group date">
                    <input type="text" class="form-control" data-date-format="yyyy-mm-dd" name="date" maxlength="10" id="date" value="<?php echo isset($_GET['date']) ? trim($_GET['date']) : ''; ?>" placeholder="">
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                    </div>
                  </div>
                </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light"><i class="ti-search"></i> Search</button>
                  </form>
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Customer</th>
                        <th>Product(s)</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th class="text-nowrap">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(empty($rows)): ?>
                        <tr>
                          <td colspan="7" align="center">No data returned.</td>
                        </tr>
                      <?php else: ?>
                        <?php foreach ($rows as $row): ?>
                          <tr>
                            <td><?php echo $sn++; ?></td>
                            <td><?php echo dashIfEmpty($row['full_name']); ?></td>
                            <td>
                              <?php $products = explode('###', $row['items']); ?>
                              <ul style="list-style-type: none;">
                                <?php foreach($products as $item_array): ?>
                                  <?php $item = explode('@@@', $item_array); //var_dump($item); die; ?>
                                    <!-- 
                                      0 - product_id
                                      1 - quantity
                                      2 - name
                                      3 - price
                                      4- image_url
                                    -->
                                    <li style="border-bottom: 1px solid #f9f9f9;">
                                      <!-- <a href="/products/<?php echo $item[0]; ?>"><?php echo $item[1]; ?> (&pound;<?php echo $item[2]; ?>)</a> -->
                                      <p style="clear: both;"><a class="thumbnail pull-left" href="admin123/products/view/<?php echo $item[0]; ?>"> <img class="media-object" src="assets/images/products/<?php echo $item[4]; ?>" style="width: 30px; height: 30px; margin-right: 5px;"> </a>&nbsp;
                                        <b><a href="admin123/products/view/<?php echo $item[0]; ?>"><?php echo $item[2]; ?></a></b>
                                        <b>&pound;<?php echo $item[3]; ?></b> x<?php echo $item[1]; ?></p>
                                      </li>
                                    <?php endforeach; ?>
                                  </ul>
                                </td>
                                <td>&pound;<?php echo number_format($row['total_price']); ?></td>
                                <td><?php echo $row['status_id'] != '4' ? '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;' . strtoupper($row['status']) . '&nbsp;</a>' : '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;' . strtoupper($row['status']) . '&nbsp;</a>'; ?><br />
                <small>Payment Mode: <?php echo $row['payment_type']; ?></small></td>
                                <td><?php echo $row['date_created'] == "0000-00-00 00:00:00" || is_null($row['date_created']) ? "-" : date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                                <td class="text-nowrap">
                                  <a href="admin123/orders/shipped/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Order Shipped"> <i class="fa fa-truck text-inverse m-r-10"></i> </a> 

                                  <a href="admin123/orders/delivered/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Order Delivered"> <i class="fa fa-gift text-inverse m-r-10"></i> </a> 

                                  <a href="admin123/orders/cancel/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Cancel Order"> <i class="fa fa-ban text-inverse m-r-10"></i> </a> 


                                  <a href="admin123/orders/delete/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a>

                                  <a href="admin123/orders/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                                </td>
                              </tr>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                <!--
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
              -->
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="col-md-3 pull-right pagination">
            <p><?php echo $links; ?></p>
          </div>
        </div>
      </div>
    </div> 
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  $('#date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });
</script>

</body>
</html>