  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <!--
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Live Chat</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "admin123/"; ?>">Dashboard</a></li>
            <li class="active">Live Chat</li>
          </ol>
        </div>
      </div>
    -->
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12" style="height:100%;width:100%">
        <div class="white-box" style="height:100%;width:100%">
          <div class="row row-in" style="height:100%;width:100%">
            <div class="col-lg-12 col-sm-12" style="height:100%;width:100%">
              <!-- <iframe src="https://dashboard.tawk.to" frameborder="0" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%"></iframe> -->
              <div style="position:relative;padding-top:56.25%;">
                <iframe src="https://dashboard.tawk.to" frameborder="0" allowfullscreen
                style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer'); echo "\n"; ?>

</body>
</html>
