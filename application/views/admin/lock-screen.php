<!DOCTYPE html>
<html>

<!-- Mirrored from themedesigner.in/demo/myadmin/myadmin-dark/lock-screen.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Jun 2016 17:41:40 GMT -->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
<title>My Admin - is a responsive admin template</title>
<!-- Bootstrap CSS -->
<link href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shiv and Respond.js IE8 support -->
<!--[if lt IE 9]>
        <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body >
<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <form class="form-horizontal m-t-20" action="http://themedesigner.in/demo/myadmin/myadmin-dark/index.html">
        <div class="form-group">
          <div class="col-xs-12 text-center">
            <div class="user-thumb text-center"> <img alt="thumbnail" class="img-responsive img-circle" src="assets/images/users/genu.jpg">
              <h3>Genelia</h3>
            </div>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="password">
          </div>
        </div>
        <div class="form-group text-center">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Login</button>
          </div>
        </div>
      </form>
    </div>
    <footer class="footer text-center">
      <div class="social"> <a href="javascript:void(0)" class="btn  btn-twitter"> <i aria-hidden="true" class="fa fa-twitter"></i> </a> <a href="javascript:void(0)" class="btn  btn-facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
      2016 © Myadmin.</footer>
  </div>
</section>
<!-- jQuery-->
<script src="assets/js/jquery.min.html"></script>
<script src="assets/js/bootstrap.min.html"></script>
<!-- jQuery Customs -->
<script src="assets/js/custom.html"></script>
<script src="assets/js/custom-widget.html"></script>
</body>

<!-- Mirrored from themedesigner.in/demo/myadmin/myadmin-dark/lock-screen.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Jun 2016 17:41:45 GMT -->
</html>
