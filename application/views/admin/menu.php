  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="<?php echo base_url() . "admin123/"; ?>" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
          <li> <a href="#" class="waves-effect"><i class="ti-pie-chart fa-fw"></i> Setup<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="admin123/countries" class="waves-effect"><i class="ti-user fa fa-globe" aria-hidden="true"></i> Countries</a> </li>
              <li> <a href="admin123/categories" class="waves-effect"><i class="ti-list fa fa-list" aria-hidden="true"></i> Categories</a> </li>
              <li> <a href="admin123/Payment-Types" class="waves-effect"><i class="ti-money fa fa-money" aria-hidden="true"></i> Payment Types</a> </li>
              <li> <a href="admin123/faqs" class="waves-effect"><i class="ti-question fa fa-question" aria-hidden="true"></i> FAQs</a> </li>
            </ul>
          </li>
          <li> <a href="admin123/products" class="waves-effect"><i class="ti-gift fa fa-gift" aria-hidden="true"></i> Products</a> </li>
          <li> <a href="admin123/orders" class="waves-effect"><i class="ti-tag fa fa-tag" aria-hidden="true"></i> Orders</a> </li>
          <li> <a href="admin123/customers" class="waves-effect"><i class="ti-user fa fa-user" aria-hidden="true"></i> Customers</a> </li>
          <li> <a href="admin123/inquiries" class="waves-effect"><i class="ti-comment fa fa-comment" aria-hidden="true"></i> Inquiries</a> </li>
          
        <?php if(($_SESSION['user_role_id'] == 1) && (strtoupper($_SESSION['user_role_name']) == "SUPER ADMINISTRATOR")): ?>
          <li> <a href="admin123/users" class="waves-effect"><i class="ti-user fa fa-user"></i> Users (Admin)</a> </li>
        <?php endif; ?>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>



<!-- 
    <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="<?php echo base_url() . "admin123/"; ?>" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
          <li> <a href="#" class="waves-effect"><i class="ti-pie-chart fa-fw"></i> Setup<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="admin123/countries" class="waves-effect"><i class="ti-user fa fa-globe" aria-hidden="true"></i> Countries</a> </li>
              <li> <a href="admin123/levels" class="waves-effect"><i class="ti-user fa fa-step-forward" aria-hidden="true"></i> Levels</a> </li>
              <li> <a href="admin123/universities" class="waves-effect"><i class="ti-location-pin fa fa-university" aria-hidden="true"></i> Universities</a> </li>
            </ul>
          </li>
          <li> <a href="admin123/orders" class="waves-effect"><i class="ti-user fa fa-tag" aria-hidden="true"></i> Orders</a> </li>
          <li> <a href="admin123/students" class="waves-effect"><i class="ti-user fa fa-user" aria-hidden="true"></i> Students</a> </li>
          
          <li> <a href="admin123/Past-Exam-Solution" class="waves-effect"><i class="ti-bar-chart fa fa-book"></i> Past Exam Solution</a> </li>
          <li> <a href="admin123/Free-Project-Proposals" class="waves-effect"><i class="ti-bar-chart fa fa-book"></i> Free Project Proposals</a> </li>
          <li> <a href="admin123/Live-Chat" class="waves-effect"><i class="ti-comment fa fa-comment"></i> Live Chat</a> </li>
          <li> <a href="https://login.mailchimp.com/" class="waves-effect" target="_blank"><i class="ti-user fa fa-users"></i> Subscribers</a> </li>
        <?php if(($_SESSION['user_role_id'] == 1) && (strtoupper($_SESSION['user_role_name']) == "SUPER ADMINISTRATOR")): ?>
          <li> <a href="admin123/users" class="waves-effect"><i class="ti-user fa fa-user"></i> Users (Admin)</a> </li>
        <?php endif; ?>
      </ul>
    </div>
  </div> -->