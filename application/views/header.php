<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <base href="<?php echo base_url();?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?> &#8211; Buy Chocolate Cakes and other variants.</title>


  <!-- Open Graph Tags -->
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:description" content="Buy Chocolate Cakes and other variants." />
  <meta property="og:url" content="http://brilloconnetz.com/ruffles/" />
  <meta property="og:site_name" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:image" content="http://brilloconnetz.com/ruffles/logo.png" />
  <meta property="og:locale" content="en_US" />
  <meta name="twitter:card" content="summary" />


  <!-- For Google -->
  <meta name="description" content="<?php echo $page_desc; ?>" />
  <meta name="keywords" content="dissertation, final year projects, project topics, essays, coursework, assignment, computer science, information technology, software engineering, management science, law students, masters, bsc, phd, thesis, abstract" />

  <meta name="author" content="brilloconnetz.com" />
  <meta name="copyright" content="<?php echo $this->config->base_url(); ?>terms-and-conditions" />
  <meta name="application-name" content="<?php echo $this->config->item('app_name'); ?>" />

  <!-- For Facebook -->
  <meta property="og:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo.png" />
  <meta property="og:url" content="<?php echo $this->config->base_url(); ?>" />
  <meta property="og:description" content="Buy Chocolate Cakes and other variants." />

  <!-- For Twitter -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:title" content="<?php echo $this->config->item('app_name'); ?>" />
  <meta name="twitter:description" content="Buy Chocolate Cakes and other variants." />
  <meta name="twitter:image" content="<?php echo $this->config->base_url(); ?>assets/images/logo.png" />
  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/images/favicon.jpg" type="image/jpg" />
  <!-- Bootstrap core CSS -->
  <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> -->
  <link rel="stylesheet" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />



  <!-- jQuery -->
  <script src="assets/vendors/jquery/jquery.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>


  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

  <!-- Custom styles for this template -->
  <link href="assets/css/shop-homepage.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body style="margin-top: 0px; padding-top: 0px;">


  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin-top: 0px;">
    <div class="container">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Ruffles</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url(); ?>">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item"><a class="nav-link" href="#">About</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Services</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Contact</a></li>
          <li class="nav-item"><a class="nav-link" href="faqs">FAQs</a></li>
          <?php if(is_logged_in()): ?>
            <li class="nav-item"><a class="nav-link" href="dashboard"><mark><span class="fa fa-home"></span> Dashboard</mark></a></li>
            <li class="nav-item"><a class="nav-link" href="logout"><span class="fa fa-sign-out"></span> Logout</a></li>
          <?php else: ?>
            <li class="nav-item"><a class="nav-link" href="register"><span class="fa fa-user"></span> Register</a></li>
            <li class="nav-item"><a class="nav-link" href="login"><span class="fa fa-sign-in"></span> Sign In</a></li>
          <?php endif; ?>


          <?php $total = 0; ?>

          <div class="dropdown" style="margin-top: -5px;">
            <button type="button" class="btn btn-info" data-toggle="dropdown">
             <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger"><strong class="item" id="cartCount"><?php echo !is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart')) ? count($this->session->userdata('cart')) : 0; ?></strong></span>
           </button>

           <div class="dropdown-menu" id="cart">
             <?php if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))): ?>
              <?php foreach($this->session->userdata('cart') as $cart): ?>
                <?php $total = $total + ($cart['product_price'] * $cart['product_quantity']); ?>
                <div class="row cart-detail cart-item">
                  <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                    <img src="assets/images/products/<?php echo $cart['product_image']; ?>" />
                  </div>
                  <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                    <p><a href="products/<?php echo $cart['product_id']; ?>"><?php echo urldecode(ellipsize($cart['product_name'], 33)); ?></a> 
                      <a href="javascript:void(0);" style="float: right;" style="float: right;" class="remove-from-cart" data-id="<?php echo $cart['product_id']; ?>"> <i class="fa fa-times-circle"></i> </a>
                    </p>
                    <span class="price text-info"> &pound;<?php echo number_format($cart['product_price']); ?></span> <span class="count"> Quantity:<?php echo number_format($cart['product_quantity']); ?></span>
                  </div>
                </div>
              <?php endforeach; ?>
            <?php else: ?>
              <div class="row cart-detail cart-item">
                <div class="col-lg-12 col-sm-12 col-12 cart-detail-product text-center">
                  <p>Your cart is empty</p>
                </div>
              </div>
            <?php endif; ?>
            <div class="row total-header-section cart-total">
              <?php if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))): ?>
                <div class="row">
                <p style="padding-left: 40px;">Total: <span class="text-info">&pound;<?php echo number_format($total); ?></span></p>
              </div>
                <div class="row">
                  <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                    <button class="btn btn-primary d-inline" onclick="location.href='cart';">View Cart</button>
                    <button class="btn btn-primary d-inline" onclick="location.href='checkout';">Checkout</button>
                  </div>
                </div>
              <?php else: ?>
                <!-- <a class="button" href="./" id="#purchase-link">Purchase Items Now</a> -->
              <?php endif; ?>

            </div>
          </div>
        </div>
      </ul>
    </div>
  </div>
</nav>

<!-- Page Content -->
<div class="container">

  <div class="row">

    <div class="col-lg-3" style="margin-top: 25px;">

      <!-- <h1 class="my-4">Ruffles</h1> -->

      <div class="list-group">
       <div class="row">
        <div id="custom-search-input">
          <?php echo form_open('products/search', 'class="form-horizontal", method="get", role="form"'); ?>
            <div class="input-group col-md-12">
              <input type="text" class="form-control input-lg" placeholder="Search a Product" name="title" value="<?php echo isset($_GET['title']) ? trim($_GET['title']) : ''; ?>" />
              <span class="input-group-btn">
                <button class="btn btn-info btn-lg" type="search">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>

      <?php foreach ($categories_all as $category): ?>
        <a href="categories/index/<?php echo $category['id']; ?>" class="list-group-item"><?php echo $category['name']; ?></a>
      <?php endforeach; ?>
    </div>

  </div>
  <!-- /.col-lg-3 -->







<!-- 




          <?php $total = 0; ?>
          <a class="cart-btn" href="javascript:void(0)"> <i class="fa fa-shopping-cart icon"></i> <strong class="item" id="cartCount"><?php echo !is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart')) ? count($this->session->userdata('cart')) : 0; ?></strong></a>
          <div class="cart" id="cart">
            <div class="cart-title">
             <h6 class="uppercase mb-0">Shopping cart</h6>
           </div>
           <?php if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))): ?>
            <?php foreach($this->session->userdata('cart') as $cart): ?>
              <?php $total += $cart['product_price']; ?>
              <div class="cart-item">
                <div class="cart-name clearfix">
                  <a href="Past-Exam-Solution?id=<?php echo $cart['product_id']; ?>"><?php echo urldecode(ellipsize($cart['product_name'], 33)); ?></a>
                  <div class="cart-price">
                    <ins>&pound;<?php echo number_format($cart['product_price']); ?></ins>
                  </div>
                </div>
                <div class="cart-close">
                  <a href="javascript:void(0);" class="remove-from-cart" data-id="<?php echo $cart['product_id']; ?>"> <i class="fa fa-times-circle"></i> </a>
                </div>
              </div>
            <?php endforeach; ?>
          <?php else: ?>
            <div class="cart-item">
              <div class="cart-name clearfix">
                <a href="Past-Exam-Solution">Your cart is empty</a>
              </div>
            </div>
          <?php endif; ?>
          <div class="cart-total">
            <?php if(!is_null($this->session->userdata('cart')) && !empty($this->session->userdata('cart'))): ?>
              <h6 class="mb-15"> Total: &pound;<?php echo number_format($total); ?></h6>
              <a class="button" href="cart">View Cart</a>
              <a class="button black" href="checkout">Checkout</a>
            <?php else: ?>
              <a class="button" href="Past-Exam-Solution" id="#purchase-link">Purchase Items Now</a>
            <?php endif; ?>
 -->