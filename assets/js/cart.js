/** Cart by Orion */

$('.add-to-cart').click(function(e){
    e.preventDefault();
    // var $btn = $(this).button('loading');
    var $btn = $(this).button();
    // $(this).prop('disabled', true);
    $(this).attr('disabled', true);
    $(this).attr('aria-disabled', true);
    
    id = $(this).data('id');
    price = $(this).data('price');
    name = $(this).data('name');
    image = $(this).data('image');

    $.ajax({
        type: 'GET',
        url: '/cart/add/' + id + '/' + price + '/' + name + '/' + image,
        success: function( data )
        {
            if(data == '1')
            {
                $('#addedToCartName').html(name);
                $('#addedToCartPrice').html(price);
                $(document).scrollTop( $("#addedToCartAlert").offset() ); 
                $('.bs-callout').removeClass('hidden').removeClass('d-none').fadeIn('slow').animate({opacity: 1.0}, 8000).delay(2000).fadeOut('slow');
            }
            else
            {
                // error 500
                $('.bs-callout').removeClass('hidden').show().delay(2000).fadeOut('slow');
            }

            // $btn.button('reset');
            // $(this).prop('disabled', false);
            $($btn).removeAttr('disabled');
            $($btn).removeAttr('aria-disabled');
        },
        error: function(xhr, status, error)
        {
            // check status && error
            // console.log(status);
            // console.log(error);
        },
        dataType: 'text'
    });

    getCartHeaderBlock();

});

// $(document).on('click', '.fClick', function(){ 
//     alert("hey!");
// }); 
// $('.remove-from-cart').click(function(e){
$(document).on('click', '.remove-from-cart', function(){ 
    // e.preventDefault();

    $(this).prop('disabled', true);
    
    id = $(this).data('id');

    $(this).parent().parent().fadeOut('slow');

    $.ajax({
        type: 'GET',
        url: '/cart/remove/' + id,
        success: function( data )
        {
            if(data == '1')
            {

                // $('#addedToCartName').html(name);
                // $('#addedToCartPrice').html(price);
                // $(document).scrollTop( $("#addedToCartAlert").offset().top ); 
                // $('.bs-callout').removeClass('hidden').fadeIn('slow').animate({opacity: 1.0}, 8000).delay(2000).fadeOut('slow');
                getCartHeaderBlock(); // cart total is also updated here
            }
            else
            {
                // error 500
                // $('.bs-callout').removeClass('hidden').show().delay(2000).fadeOut('slow');
            }
        },
        error: function(xhr, status, error)
        {
            // check status && error
            // console.log(status);
            // console.log(error);
        },
        dataType: 'text'
    });
});

/* Update Cart */

$(document).on('click', '.update-cart', function(){ 
    // e.preventDefault();

    $(this).prop('disabled', true);
    
    id = $(this).data('id');
    cart_quantity = $(this).data('cart-quantity');

    $.ajax({
        type: 'GET',
        url: '/cart/update/' + id + '/' + $('#' + cart_quantity).val(),
        success: function( data )
        {
            if(data == '1')
            {
                // refresh
                location.reload(true);
            }
            else
            {
            }
        },
        error: function(xhr, status, error)
        {
        },
        dataType: 'text'
    });
});

function getCartHeaderBlock()
{
    $.ajax({
        url:'cart/fetch',
        type:'get',
        cache: false,
        dataType: 'json',
        success:function(response){
            // console.log(response);
            // console.log("cart_items datatype", typeof(response.cart_items));
            if(response.cart_items != null && response.cart_items.length > 0 ) // has items in cart
            {
                // empty cart header
                $('#cart .cart-item').remove();
                $('#cart .cart-total').remove();

                // clear cart count
                $('#cartCount').text(0);

                // add cart item lines to cart header
                for (var i = 0; i < response.cart_items.length; i++)
                {
                    item = response.cart_items[i][0]; // console.log(item);

                    $("#cart").append('<div class="row cart-detail cart-item"><div class="col-lg-4 col-sm-4 col-4 cart-detail-img"><img src="assets/images/products/' + item.product_image + 
                        '" /></div><div class="col-lg-8 col-sm-8 col-8 cart-detail-product"><p><a href="products/' + 
                        item.product_id + '">'
                        + item.product_name_short
                        + '</a>' + '<a href="javascript:void(0);" style="float: right;" style="float: right;" class="remove-from-cart" data-id="' + item.product_id + '"> <i class="fa fa-times-circle"></i> </a></p><span class="price text-info"> &pound;' + 
                        item.product_price + '</span> <span class="count"> Quantity:' + item.product_quantity + '</span></div></div>');

                }

                // add cart total line to cart header
                $("#cart").append('<div class="row total-header-section cart-total"><div class="row"><p style="padding-left: 40px;">Total: <span class="text-info">&pound;' + 
                    response.cart_total + '</span></p></div><div class="row"><div class="col-lg-12 col-sm-12 col-12 text-center checkout"><button class="btn btn-primary d-inline" onclick="location.href=\'cart\';">View Cart</button><button class="btn btn-primary d-inline" onclick="location.href=\'checkout\';">Checkout</button></div></div></div>'
                );


                // add cart count
                $('#cartCount').text(response.cart_items.length);

                // add cart total on Cart page
                $('#cartTotal').text(response.cart_total);

                // if we're on cart page and there are no more items refresh page
                var current_page = location.pathname.substring(1);

                if(current_page == "cart" && response.cart_items.length == 0)
                {
                    // refresh
                    location.reload(true);
                }
            }
            else // has no item in cart
            {
                // empty cart header
                $('#cart .cart-item').remove();
                $('#cart .cart-total').remove();

                // clear cart count
                $('#cartCount').text(0);

                // add "cart is empty" line
                $("#cart").append('<div class="row cart-detail cart-item"><div class="col-lg-12 col-sm-12 col-12 cart-detail-product text-center"><p>Your cart is empty</p></div></div>');
                
                

                // add "purchase items now" link
                // $("#cart").append('<div class="cart-total"><a class="button" href="/" id="#purchase-link">Purchase Items Now</a></div>');

                // add cart total on Cart page
                $('#cartTotal').text(response.cart_total);

                // if we're on cart page and there are no more items, refresh page
                var current_page = location.pathname.substring(1);

                if(current_page == "cart" && response.cart_items.length == 0)
                {
                    // refresh
                    location.reload(true);
                }
            }

            $("#ajaxloader").hide();
            $("#contactform").show();

            $("#formmessage").html(response).show().delay(20000).fadeOut('slow');
      }
  });
    // cart-items: array(product_id, product_name, number_format($cart['product_price']))
    // cart-total: total(number_format(array_sum($this->session->userdata('cart'))))

}

// var shortText = jQuery.trim(title).substring(0, 10).split(" ").slice(0, -1).join(" ") + "...";

String.prototype.trimToLength = function(m) {
  return (this.length > m) 
    ? jQuery.trim(this).substring(0, m).split(" ").slice(0, -1).join(" ") + "..."
    : this;
};