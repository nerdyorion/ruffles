// var CREATE_PAYMENT_URL  = 'http://academicianhelp.test/checkout/paypal';
// var EXECUTE_PAYMENT_URL = 'http://academicianhelp.test/checkout/paypal';

var CREATE_PAYMENT_URL  = window.location.protocol + '//' + window.location.hostname + '/checkout/paypal/createPayment';
var EXECUTE_PAYMENT_URL  = window.location.protocol + '//' + window.location.hostname + '/checkout/paypal/executePayment';

// Sandbox. https://api.sandbox.paypal.com
// Live. https://api.paypal.com

// Access token: /v1/oauth2/token


paypal.Button.render({
  // env: 'production', // Or 'sandbox',
  env: 'production', // Or 'sandbox',

  commit: true, // Show a 'Pay Now' button

  style: {
    color: 'gold',
    size: 'small'
  },

  payment: function(data, actions) {
    /* 
     * Set up the payment here 
    */
    return paypal.request.post(CREATE_PAYMENT_URL).then(function(data) {
      return data.id;
    });
  },

  onAuthorize: function(data, actions) {
    /* 
     * Execute the payment here 
    */
    return paypal.request.post(EXECUTE_PAYMENT_URL, {
      paymentID: data.paymentID,
      payerID:   data.payerID
    }).then(function() {
      // The payment is complete!
      // You can now show a confirmation message to the customer
    });
  },

  onCancel: function(data, actions) {
    /* 
     * Buyer cancelled the payment 
    */
  },

  onError: function(err) {
    /* 
     * An error occurred during the transaction 
    */
  }
}, '#paypal-button');