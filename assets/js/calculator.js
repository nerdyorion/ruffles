//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 2).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 2).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled');
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }

    calculatePrice();
});
$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
    // Allow: Ctrl+A
    (e.keyCode == 65 && e.ctrlKey === true) || 
    // Allow: home, end, left, right 
    (e.keyCode >= 35 && e.keyCode <= 39))
    {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$('#currencyBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');

    calculatePrice();
})

$('#deadlineBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');

    calculatePrice();
})

function calculatePrice()
{
    var currency = $("#currency").val();
    var deadline = parseInt($("#deadline").val());
    var pages = parseInt($("#pages").val());

    var words_per_page = 250;
    var base_price = 50;
    var word_count = '500 Words';
    var totalPriceGBP = 0;
    var totalPrice = 0;

    if(deadline == 1) // 1day = 50
    {
        base_price = 50;
    }
    else if(deadline == 2) // 2-3 days = 48
    {
        base_price = 48;
    }
    else if(deadline == 4) // 4-5 days = 46
    {
        base_price = 46;
    }
    else if(deadline == 6) // 6-8 days = 44
    {
        base_price = 44;
    }
    else if(deadline == 9) // >9 days = 42
    {
        base_price = 42;
    }
    else if(deadline == 20) // >20 days = 40
    {
        base_price = 40;
    }
    else
    {
        base_price = 50;
        alert('Unable to calculate cost. Please reload page.');
    }

    word_count = pages * words_per_page;
    $("#wordCount").text(numberWithCommas(word_count) + ' Words');

    totalPriceGBP = ( base_price * (pages / 2.0) ) - 2; // in GBP

    // NOW IF PRICES ARE DEFAULT 2 PAGES
    if(deadline == 1 && pages == 2) // 1day = 50
    {
        totalPriceGBP = 50;
    }
    else if(deadline == 2 && pages == 2) // 2-3 days = 48
    {
        totalPriceGBP = 48;
    }
    else if(deadline == 4 && pages == 2) // 4-5 days = 46
    {
        totalPriceGBP = 46;
    }
    else if(deadline == 6 && pages == 2) // 6-8 days = 44
    {
        totalPriceGBP = 44;
    }
    else if(deadline == 9 && pages == 2) // >9 days = 42
    {
        totalPriceGBP = 42;
    }
    else if(deadline == 20 && pages == 2) // >20 days = 40
    {
        totalPriceGBP = 40;
    }
    else
    {
        // use calculated price which has considered no_of_pages
    }

    // Final calculation based on currency
    if(currency == "GBP")
    {
        totalPrice = totalPriceGBP;
        totalPrice = roundTo(totalPrice, 2);
    }
    else if(currency == "USD")
    {
        totalPrice = totalPriceGBP * 1.41;
        totalPrice = roundTo(totalPrice, 2);
    }
    else if(currency == "EUR")
    {
        totalPrice = totalPriceGBP * 1.14;
        totalPrice = roundTo(totalPrice, 2);
    }
    else
    {
        totalPrice = "-";
        alert('Unable to calculate cost. Please reload page.');
    }

    $("#totalPrice").text(numberWithCommas(totalPrice) + ' ' + currency);
    // default currency - GBP.

    // 1 GBP = 1.41 USD
    // 1 GBP = 1.14 EURO

    // ((basedprice *(pages/2))-2)



    // alert(" currency: " + currency + "\n deadline: " + deadline + "\n pages: " + pages + "\n\n base_price: " + base_price);
}

const numberWithCommas = (x) => {
  // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function roundTo(n, digits) {
    var negative = false;
    if (digits === undefined) {
        digits = 0;
    }
        if( n < 0) {
        negative = true;
      n = n * -1;
    }
    var multiplicator = Math.pow(10, digits);
    n = parseFloat((n * multiplicator).toFixed(11));
    n = (Math.round(n) / multiplicator).toFixed(2);
    if( negative ) {    
        n = (n * -1).toFixed(2);
    }
    return n;
}